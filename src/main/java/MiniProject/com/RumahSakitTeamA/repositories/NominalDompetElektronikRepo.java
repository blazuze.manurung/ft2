package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.M_Wallet_Default_Nominal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NominalDompetElektronikRepo extends JpaRepository<M_Wallet_Default_Nominal, Long> {
    @Query(value = "SELECT * FROM m_wallet_default_nominal WHERE id = :id", nativeQuery = true)
    M_Wallet_Default_Nominal findByIdData(long id);
    @Query(value = "SELECT * FROM m_wallet_default_nominal WHERE is_delete = false", nativeQuery = true)
    List<M_Wallet_Default_Nominal> findAllNotDeleted();
    @Query(value = "SELECT * FROM m_wallet_default_nominal WHERE nominal = ?1 AND is_delete = false", nativeQuery = true)
    List<M_Wallet_Default_Nominal> SearchNominalDompetElektronik(Integer keyword);
}
