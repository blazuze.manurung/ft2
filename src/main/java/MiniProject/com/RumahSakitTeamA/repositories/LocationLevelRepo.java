package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.LocationLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LocationLevelRepo extends JpaRepository<LocationLevel, Long> {
    @Query(value = "SELECT * FROM m_location_level WHERE id = :id", nativeQuery = true)
    LocationLevel findByIdData(long id);
    @Query(value = "SELECT * FROM m_location_level WHERE is_delete = false ORDER BY name ASC" , nativeQuery = true)
    List<LocationLevel> findAllNotDeleted();
    @Query(value = "FROM LocationLevel WHERE lower(name) LIKE lower(concat('%',?1,'%')) AND is_Delete = false")
    List<LocationLevel> SearchLocationLevel(String keyword);
    @Query("SELECT COUNT(*) FROM LocationLevel WHERE lower(name) = lower(?1) AND is_delete = false")
    int countByNameIgnoreCaseAndNotDeleted(String name);
    @Query("SELECT COUNT(*) FROM LocationLevel WHERE lower(name) = lower(?1) AND is_delete = false")
    int countByAbbreviationIgnoreCaseAndNotDeleted(String abbreviation);
    @Query("SELECT COUNT(*) FROM LocationLevel WHERE lower(name) = lower(?1) AND is_delete = false AND id != ?2")
    int countByNameIgnoreCaseAndNotDeletedAndIdNot(String name, Long id);
    @Query("SELECT COUNT(*) FROM LocationLevel WHERE lower(abbreviation) = lower(?1) AND is_delete = false AND id != ?2")
    int countByAbbreviationIgnoreCaseAndNotDeletedAndIdNot(String abbreviation, Long id);
    @Query("FROM LocationLevel WHERE lower(name) = lower(?1) AND is_delete = false")
    List<LocationLevel> findDuplicatesByNameIgnoreCaseAndNotDeleted(String name);
    @Query("FROM LocationLevel WHERE lower(abbreviation) = lower(?1) AND is_delete = false")
    List<LocationLevel> findDuplicatesByAbbreviationIgnoreCaseAndNotDeleted(String abbreviation);


}
