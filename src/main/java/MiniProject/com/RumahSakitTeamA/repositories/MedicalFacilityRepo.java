package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.models.MedicalFacility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MedicalFacilityRepo extends JpaRepository<MedicalFacility, Long> {
    @Query(value = "SELECT * FROM m_medical_facility WHERE id = :id", nativeQuery = true)
    MedicalFacility findByIdData(long id);
}
