package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.DokterTreatment;
import MiniProject.com.RumahSakitTeamA.models.ProfilDokterShare;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProfilDokterShareRepo extends JpaRepository<ProfilDokterShare, Long> {
    @Query(value = "SELECT * FROM m_doctor_profil WHERE id = :id", nativeQuery = true)
    ProfilDokterShare findByIdData (long id);

    @Query(value = "SELECT * FROM m_doctor_profil WHERE id= :id", nativeQuery = true)
    List<ProfilDokterShare> findByIdProfil(Long id);

    @Query(value = "SELECT * FROM m_doctor_profil WHERE biodata_id= :id " , nativeQuery = true)
    List<ProfilDokterShare> findByIdDoctor(Long id);

    @Query(value = "SELECT * FROM m_doctor_profil WHERE is_delete = false" , nativeQuery = true)
    List<ProfilDokterShare> findAllDoctor();
}
