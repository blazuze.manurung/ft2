package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.models.LevelEducation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LevelEducationRepo extends JpaRepository<LevelEducation, Long> {
    @Query(value = "SELECT * FROM m_education_level WHERE id = :id", nativeQuery = true)
    LevelEducation findByIdData(long id);
}
