package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.models.DoctorOffice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DoctorOfficeRepo extends JpaRepository<DoctorOffice, Long> {
    @Query(value = "SELECT * FROM t_doctor_office WHERE id = :id", nativeQuery = true)
    DoctorOffice findByIdData(long id);
}
