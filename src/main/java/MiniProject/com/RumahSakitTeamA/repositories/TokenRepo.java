package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.TokenEmail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface TokenRepo extends JpaRepository<TokenEmail,Long> {
    @Query(value = "SELECT * FROM t_token WHERE lower(t_token.email) = lower(:email) " +
            "AND lower(t_token.used_for) = lower(:used_for)" +
            "AND t_token.token = :input ORDER BY t_token.expired_on DESC LIMIT 1", nativeQuery = true)
    Optional<TokenEmail> getLastToken(String email, String used_for, String input);

    @Query(value = "SELECT * FROM t_token WHERE lower(t_token.email) = lower(:email) " +
            "AND lower(t_token.used_for) = lower(:used_for) ORDER BY t_token.expired_on DESC LIMIT 1", nativeQuery = true)
    TokenEmail getLastToken(String email, String used_for);
}
