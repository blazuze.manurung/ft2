package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.M_Customer_Member;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CustomerMemberRepo extends JpaRepository<M_Customer_Member, Long> {
    @Query(value = "SELECT * FROM m_customer_member " +
                   "JOIN m_customer ON m_customer_member.customer_id = m_customer.id " +
                   "JOIN m_biodata ON m_customer.biodata_id = m_biodata.id " +
                   "WHERE m_biodata.is_delete = false " +
                   "AND m_customer.is_delete = false " +
                   "AND m_customer_member.is_delete = false " +
                   "ORDER BY m_biodata.fullname ASC", nativeQuery = true)
    List<M_Customer_Member> findAllNotDeleted();
    @Query(value = "SELECT * FROM m_customer_member " +
            "JOIN m_customer ON m_customer_member.customer_id = m_customer.id " +
            "JOIN m_biodata ON m_customer.biodata_id = m_biodata.id " +
            "WHERE m_biodata.is_delete = false " +
            "AND m_customer.is_delete = false " +
            "AND m_customer_member.is_delete = false " +
            "ORDER BY m_biodata.fullname ASC", nativeQuery = true)
    Page<M_Customer_Member> findAllNotDeletedPageOrder();
    @Query(value = "SELECT * FROM m_customer_member WHERE parent_biodata_id = :id",nativeQuery = true)
    List<M_Customer_Member> findAllCustomerMemberByParentId(Long id);
    @Query(value = "SELECT * FROM m_customer_member WHERE id = :id", nativeQuery = true)
    M_Customer_Member findByIdData(Long id);
    @Query(value = "SELECT * FROM m_customer_member " +
                   "JOIN m_customer ON m_customer_member.customer_id = m_customer.id " +
                   "JOIN m_biodata ON m_customer.biodata_id = m_biodata.id " +
                   "WHERE lower(fullname) LIKE lower(concat('%',?1,'%')) " +
                   "AND m_biodata.is_delete = false " +
                   "AND m_customer.is_delete = false " +
                   "AND m_customer_member.is_delete = false", nativeQuery = true)
    List<M_Customer_Member> SearchCustomerMemberByFullname(String keyword);
}