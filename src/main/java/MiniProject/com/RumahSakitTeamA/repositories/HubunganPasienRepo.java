package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.M_Customer_Relation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HubunganPasienRepo extends JpaRepository<M_Customer_Relation, Long> {
    @Query(value = "SELECT * FROM m_customer_relation WHERE id = :id", nativeQuery = true)
    M_Customer_Relation findByIdData(long id);
    @Query(value = "SELECT * FROM m_customer_relation WHERE is_delete = false", nativeQuery = true)
    List<M_Customer_Relation> findAllNotDeleted();
    @Query(value = "SELECT * FROM HubunganPasien WHERE lower(name) LIKE lower(concat('%',?1,'%')) AND is_Delete = false", nativeQuery = true)
    List<M_Customer_Relation> SearchHubunganPasien(String keyword);
}
