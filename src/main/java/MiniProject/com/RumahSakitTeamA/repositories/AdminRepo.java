package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
public interface AdminRepo extends JpaRepository<Admin, Long>{
}
