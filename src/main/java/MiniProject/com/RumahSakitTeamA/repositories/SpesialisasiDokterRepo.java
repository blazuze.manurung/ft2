package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.SpesialisasiDokter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SpesialisasiDokterRepo extends JpaRepository <SpesialisasiDokter, Long> {

    @Query(value = "SELECT * FROM m_specialization ORDER BY name ASC", nativeQuery = true)
    List<SpesialisasiDokter> FindAllSpesialisasiDokterOrderByASC();

    @Query(value = "SELECT * FROM m_specialization WHERE id = :id", nativeQuery = true)
    SpesialisasiDokter findByIdData(long id);

    //  pencarian
    @Query("FROM SpesialisasiDokter WHERE lower(name) LIKE lower(concat('%',?1,'%') ) ")
    List<SpesialisasiDokter> SearchSpesialisasiDokter(String keyword);

    @Query(value = "SELECT * FROM m_specialization WHERE is_delete = false ORDER BY name ASC", nativeQuery = true)
    Page<SpesialisasiDokter> FindAllSpesialisasiDokterOrderByASC(Pageable pageable);
}
