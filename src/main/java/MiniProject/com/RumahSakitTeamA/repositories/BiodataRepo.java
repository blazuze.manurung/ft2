package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BiodataRepo extends JpaRepository<Biodata, Long> {
    @Query(value = "SELECT * FROM m_biodata WHERE id = :id", nativeQuery = true)
    Biodata findByIdData(long id);
}
