package MiniProject.com.RumahSakitTeamA.repositories;

import MiniProject.com.RumahSakitTeamA.models.Location;
import MiniProject.com.RumahSakitTeamA.models.LocationLevel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LocationRepo extends JpaRepository<Location, Long> {
    @Query(value = "SELECT * FROM m_location WHERE is_delete = false ORDER BY id ASC", nativeQuery = true)
    List<Location> findAllNotDeleted();
    @Query(value = "SELECT * FROM m_location WHERE id = :id", nativeQuery = true)
    Location findByIdData(long id);
    @Query(value = "SELECT * FROM m_location WHERE name = :name AND parent_id = :parentId AND location_level_id = :locationLevelId", nativeQuery = true)
    Location findByNameAndParentIdAndLocationLevelId(String name, Long parentId, Long locationLevelId);
    @Query("FROM Location WHERE lower(name) LIKE lower(concat('%',?1,'%')) AND is_Delete = false")
    List<Location> SearchLocation(String keyword);
    @Query(value = "SELECT * FROM m_location WHERE name = :name AND location_level_id = :locationLevelId", nativeQuery = true)
    Location findByNameAndLocationLevelId(String name, Long locationLevelId);
    @Query(value = "SELECT * FROM m_location WHERE name = :name AND parent_id = :parentId AND location_level_id = :locationLevelId AND id != :id", nativeQuery = true)
    Location findByNameAndParentIdAndLocationLevelIdAndIdNot(String name, Long parentId, Long locationLevelId, Long id);
    @Query(value = "SELECT * FROM m_location WHERE name = :name AND location_level_id = :locationLevelId AND id != :id", nativeQuery = true)
    Location findByNameAndLocationLevelIdAndIdNot(String name, Long locationLevelId, Long id);
    @Query("SELECT l FROM Location l WHERE l.parentLocation = :parentLocation AND l.is_Delete = false")
    List<Location> findByParentLocation(@Param("parentLocation") Location parentLocation);
    @Query("SELECT l FROM Location l WHERE l.locationLevel.id = :locationLevelId AND l.is_Delete = false")
    List<Location> findByLocationLevelId(@Param("locationLevelId") Long locationLevelId);
}

