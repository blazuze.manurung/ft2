package MiniProject.com.RumahSakitTeamA.models;

import javax.persistence.*;

@Entity
@Table(name = "m_doctor_profil")
public class ProfilDokterShare extends BaseProperties{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    public Biodata biodata;

    @Column(name = "biodata_id")
    private long biodata_id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public BiodataDokter biodataDokter;

    @Column(name = "doctor_id")
    private long doctor_id;

    @ManyToOne
    @JoinColumn(name = "doctor_office_id", insertable = false, updatable = false)
    public DoctorOffice doctorOffice;

    @Column(name = "doctor_office_id")
    private long doctor_office_id;

    @ManyToOne
    @JoinColumn(name = "doctor_education_id", insertable = false, updatable = false)
    public DokterEducation dokterEducation;

    @Column(name = "doctor_education_id")
    private long doctor_education_id;

    @ManyToOne
    @JoinColumn(name = "doctor_treatment_id", insertable = false, updatable = false)
    public DokterTreatment dokterTreatment;

    @Column(name = "doctor_treatment_id")
    private long doctor_treatment_id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public long getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(long biodata_id) {
        this.biodata_id = biodata_id;
    }

    public BiodataDokter getBiodataDokter() {
        return biodataDokter;
    }

    public void setBiodataDokter(BiodataDokter biodataDokter) {
        this.biodataDokter = biodataDokter;
    }

    public long getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(long doctor_id) {
        this.doctor_id = doctor_id;
    }

    public DoctorOffice getDoctorOffice() {
        return doctorOffice;
    }

    public void setDoctorOffice(DoctorOffice doctorOffice) {
        this.doctorOffice = doctorOffice;
    }

    public long getDoctor_office_id() {
        return doctor_office_id;
    }

    public void setDoctor_office_id(long doctor_office_id) {
        this.doctor_office_id = doctor_office_id;
    }

    public DokterEducation getDokterEducation() {
        return dokterEducation;
    }

    public void setDokterEducation(DokterEducation dokterEducation) {
        this.dokterEducation = dokterEducation;
    }

    public long getDoctor_education_id() {
        return doctor_education_id;
    }

    public void setDoctor_education_id(long doctor_education_id) {
        this.doctor_education_id = doctor_education_id;
    }

    public DokterTreatment getDokterTreatment() {
        return dokterTreatment;
    }

    public void setDokterTreatment(DokterTreatment dokterTreatment) {
        this.dokterTreatment = dokterTreatment;
    }

    public long getDoctor_treatment_id() {
        return doctor_treatment_id;
    }

    public void setDoctor_treatment_id(long doctor_treatment_id) {
        this.doctor_treatment_id = doctor_treatment_id;
    }
}
