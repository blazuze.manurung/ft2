package MiniProject.com.RumahSakitTeamA.models;

import javax.persistence.*;

@Entity
@Table(name = "m_customer_member")
public class M_Customer_Member extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "parent_biodata_id", insertable = false, updatable = false)
    public Biodata biodata;

    @Column(name = "parent_biodata_id")
    private long parent_biodata_id;

    @ManyToOne
    @JoinColumn(name = "customer_id", insertable = false, updatable = false)
    public M_Customer customer;

    @Column(name = "customer_id")
    private long customer_id;

    @ManyToOne
    @JoinColumn(name = "customer_relation_id", insertable = false, updatable = false)
    public M_Customer_Relation hubunganPasien;

    @Column(name = "customer_relation_id")
    private long customer_relation_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public long getParent_biodata_id() {
        return parent_biodata_id;
    }

    public void setParent_biodata_id(long parent_biodata_id) {
        this.parent_biodata_id = parent_biodata_id;
    }

    public M_Customer getCustomer() {
        return customer;
    }

    public void setCustomer(M_Customer customer) {
        this.customer = customer;
    }

    public long getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(long customer_id) {
        this.customer_id = customer_id;
    }

    public M_Customer_Relation getHubunganPasien() {
        return hubunganPasien;
    }

    public void setHubunganPasien(M_Customer_Relation hubunganPasien) {
        this.hubunganPasien = hubunganPasien;
    }

    public long getCustomer_relation_id() {
        return customer_relation_id;
    }

    public void setCustomer_relation_id(long customer_relation_id) {
        this.customer_relation_id = customer_relation_id;
    }
}
