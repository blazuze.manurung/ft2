package MiniProject.com.RumahSakitTeamA.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "m_customer")
public class M_Customer extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    public Biodata biodata;

    @Column(name = "biodata_id")
    private Long biodata_id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Column(name = "dob")
    private Date dob;

    @Column(name = "gender", length = 1)
    private String gender;

    @ManyToOne
    @JoinColumn(name = "blood_group_id", insertable = false, updatable = false)
    public GolonganDarah golonganDarah;

    @Column(name = "blood_group_id")
    private Long blood_group_id;

    @Column(name = "rhesus_type", length = 5)
    private String  rhesus_Type;

    @Column(name = "height")
    private Double height;

    @Column(name = "weight")
    private Double weight;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public Long getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(Long biodata_id) {
        this.biodata_id = biodata_id;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public GolonganDarah getGolonganDarah() {
        return golonganDarah;
    }

    public void setGolonganDarah(GolonganDarah golonganDarah) {
        this.golonganDarah = golonganDarah;
    }

    public Long getBlood_group_id() {
        return blood_group_id;
    }

    public void setBlood_group_id(Long blood_group_id) {
        this.blood_group_id = blood_group_id;
    }

    public String getRhesus_Type() {
        return rhesus_Type;
    }

    public void setRhesus_Type(String rhesus_Type) {
        this.rhesus_Type = rhesus_Type;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
