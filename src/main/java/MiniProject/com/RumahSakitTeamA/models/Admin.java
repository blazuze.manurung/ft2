package MiniProject.com.RumahSakitTeamA.models;

import javax.persistence.*;

@Entity
@Table(name = "m_admin")
public class Admin extends BaseProperties {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    private Biodata m_biodata;

    @Column(name = "biodata_id", nullable = true)
    private Long biodata_id;

    @Column(name = "code", nullable = true, length = 10)
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Biodata getM_biodata() {
        return m_biodata;
    }

    public void setM_biodata(Biodata m_biodata) {
        this.m_biodata = m_biodata;
    }

    public Long getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(Long biodata_id) {
        this.biodata_id = biodata_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
