package MiniProject.com.RumahSakitTeamA.models;

import javax.persistence.*;

@Entity
@Table(name = "m_doctor_education")
public class DokterEducation extends BaseProperties{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    public Biodata biodata;

    @Column(name = "biodata_id")
    private long biodata_id;

    @ManyToOne
    @JoinColumn(name = "education_level_id", insertable = false, updatable = false)
    public LevelEducation levelEducation;

    @Column(name = "education_level_id")
    private long education_level_id;

    @Column(name = "institution_name", length = 100)
    private String institution_name;

    @Column(name = "major", length = 100)
    private String major;

    @Column(name = "start_year", length = 4)
    private String start_year;

    @Column(name = "end_year", length = 4)
    private String end_year;

    @Column(name = "is_last_education" )
    private boolean is_last_education;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public long getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(long biodata_id) {
        this.biodata_id = biodata_id;
    }

    public LevelEducation getLevelEducation() {
        return levelEducation;
    }

    public void setLevelEducation(LevelEducation levelEducation) {
        this.levelEducation = levelEducation;
    }

    public long getEducation_level_id() {
        return education_level_id;
    }

    public void setEducation_level_id(long education_level_id) {
        this.education_level_id = education_level_id;
    }

    public String getInstitution_name() {
        return institution_name;
    }

    public void setInstitution_name(String institution_name) {
        this.institution_name = institution_name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getStart_year() {
        return start_year;
    }

    public void setStart_year(String start_year) {
        this.start_year = start_year;
    }

    public String getEnd_year() {
        return end_year;
    }

    public void setEnd_year(String end_year) {
        this.end_year = end_year;
    }

    public boolean isIs_last_education() {
        return is_last_education;
    }

    public void setIs_last_education(boolean is_last_education) {
        this.is_last_education = is_last_education;
    }
}
