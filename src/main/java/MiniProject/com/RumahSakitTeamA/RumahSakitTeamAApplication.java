package MiniProject.com.RumahSakitTeamA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RumahSakitTeamAApplication {

	public static void main(String[] args) {
		SpringApplication.run(RumahSakitTeamAApplication.class, args);
	}

}
