package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("locationlevel")
public class LocationLevelController {
    @RequestMapping("")
    public String locationLevel(){ return ("locationlevel/locationlevel"); }

    @RequestMapping("addlocationlevel")
    public String addLocationLevel(){ return "locationlevel/addlocationlevel";}

    @RequestMapping("editlocationlevel/{id}")
    public String editLocationLevel(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "locationlevel/editlocationlevel";
    }

    @RequestMapping("deletelocationlevel/{id}")
    public String deleteLocationLevel(@PathVariable("id") Long id, Model model) {
        model.addAttribute("id", id);
        return "locationlevel/deletelocationlevel";
    }


}


