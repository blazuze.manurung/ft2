package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LandingPageController {
	@RequestMapping("")
    public String LandingPage()
	{
		return "landingpage";
	}
	@RequestMapping("/register")
	public String register() {return "register/register";}

	@RequestMapping("/register/otp/{email}")
	public String registerOTP(@PathVariable("email")String email, Model model){
		model.addAttribute(email);
		return "register/otp-register";
	}

	@RequestMapping("/register/setPassword/{email}")
	public String passwordRegister(@PathVariable("email") String email, Model model){
		model.addAttribute(email);
		return "register/set-userdata";
	}
	@RequestMapping("/register/setUserData/{email}&{password}")
	public String registerUserData(@PathVariable("email") String email, @PathVariable("password") String password, Model model){
		model.addAttribute(email);
		model.addAttribute(password);
		return "register/set-userdata";
	}

	@RequestMapping("/register/info-register")
	public String registerInfo(){
		return "register/info-register";
	}
}
