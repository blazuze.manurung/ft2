package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("doktertreatment")
public class DokterTreatmentController {

    @RequestMapping("")
    public String dokterTreatment(){
        return "doktertreatment/doktertreatment";
    }

    @RequestMapping("adddoktertreatment")
    public String addDokterTreatment(){
        return "doktertreatment/adddoktertreatment";
    }

    @RequestMapping("adddoktertreatment/{id}")
    public String addDokterTreatment(@PathVariable("id")Long id, Model model){
        model.addAttribute("id",id);
        return "doktertreatment/adddoktertreatment";
    }

    @RequestMapping("editdoktertreatment/{id}")
    public String editDokterTreatment(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "doktertreatment/editdoktertreatment";
    }

    @RequestMapping("deletedoktertreatment/iddokter={id_dokter}&idtreatment={id_treatment}")
    public String deleteDokterTreatment(@PathVariable("id_dokter") Long id_dokter, @PathVariable("id_treatment") Long id_treatment ,Model model){
        model.addAttribute("id_dokter", id_dokter);
        model.addAttribute("id_treatment", id_treatment);
        return "doktertreatment/deletedoktertreatment";
    }
}
