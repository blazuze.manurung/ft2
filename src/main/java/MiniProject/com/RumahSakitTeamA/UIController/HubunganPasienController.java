package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("hubunganpasien")
public class HubunganPasienController {
    @RequestMapping("")
    public String hubunganPasien(){
        return "hubunganpasien/hubunganpasien";
    }

    @RequestMapping("addhubunganpasien")
    public String addHubunganPasien(){
        return "hubunganpasien/addhubunganpasien";
    }

    @RequestMapping("edithubunganpasien/{id}")
    public String editHubunganPasien(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "hubunganpasien/edithubunganpasien";
    }

    @RequestMapping("deletehubunganpasien/{id}")
    public String deleteHubunganPasien(@PathVariable("id") Long id, Model model){
        model.addAttribute("id", id);
        return "hubunganpasien/deletehubunganpasien";
    }
}
