package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("biodatadokter")
public class BiodataDokterController {
    @RequestMapping("")
    public String biodataDokter(){
        return "biodatadokter/biodatadokter";
    }

    @RequestMapping("addbiodatadokter")
    public String addBiodataDokter(){
        return "biodatadokter/addbiodatadokter";
    }

    @RequestMapping("editbiodatadokter/{id}")
    public String editBiodataDokter(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "biodatadokter/editbiodatadokter";
    }

    @RequestMapping("deletebiodatadokter/{id}")
    public String deleteBiodataDokter(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "biodatadokter/deletebiodatadokter";
    }
}
