package MiniProject.com.RumahSakitTeamA.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("profildoktershare")
public class ProfilDokterShareController {
    @RequestMapping("")
    public String profileDokterShare(){
        return "profildoktershare/profildoktershare";
    }

    @RequestMapping("addprofildoktershare")
    public String addProfileDokterShare(){
        return "profildoktershare/addprofildoktershare";
    }

    @RequestMapping("info/{id}")
    public String infoProfileDokterShare(@PathVariable("id")Long id, Model model){
        model.addAttribute(id);
        return "profildoktershare/info-treatment";
    }

    @RequestMapping("editprofildoktershare/{id}")
    public String editProfileDokterShare(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "profildoktershare/editprofildoktershare";
    }

    @RequestMapping("deleteprofildoktershare/{id}")
    public String deleteProfileDokterShare(@PathVariable("id") Integer id, Model model){
        model.addAttribute("id", id);
        return "profildoktershare/deleteprofildoktershare";
    }
}
