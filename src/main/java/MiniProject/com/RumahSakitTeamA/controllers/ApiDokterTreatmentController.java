package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.DokterTreatment;
import MiniProject.com.RumahSakitTeamA.repositories.DokterTreatmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiDokterTreatmentController {

    @Autowired
    private DokterTreatmentRepo dokterTreatmentRepo;

    @GetMapping("/getalldoktertreatment")
    public ResponseEntity<List<DokterTreatment>> GetAllDokterTreatment()
    {
        try {
            List<DokterTreatment> doktertreatment = this.dokterTreatmentRepo.findAll();
            return new ResponseEntity<>(doktertreatment, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyiddoktertreatment/{id}")
    public ResponseEntity<List<DokterTreatment>> GetDokterTreatmentById(@PathVariable("id") Long id)
    {
        try {
            List<DokterTreatment> doktertreatment = this.dokterTreatmentRepo.FindBYDoctorID(id);
            return new ResponseEntity<>(doktertreatment, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyiddoktertindakan/{id}")
    public ResponseEntity<Object> GetTindakanById(@PathVariable("id") Long id)
    {
        try {
            Optional<DokterTreatment> doktertreatment = this.dokterTreatmentRepo.findProfil(id);
            return new ResponseEntity<>(doktertreatment, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }



    @PostMapping("/adddoktertreatment")
    public ResponseEntity<Object> SaveDokterTreatment(@RequestBody DokterTreatment doktertreatment)
    {
        try {
            doktertreatment.setCreated_By(1L);
            doktertreatment.setCreated_On(new Date());
            this.dokterTreatmentRepo.save(doktertreatment);
            return new ResponseEntity<>(doktertreatment, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("editdoktertreatment/{id}")
    public ResponseEntity<Object> EditDokterTreatment(@RequestBody DokterTreatment doktertreatment, @PathVariable("id") Long id)
    {
        Optional<DokterTreatment> doktertreatmentData = this.dokterTreatmentRepo.findById(id);

        if (doktertreatmentData.isPresent())
        {
            doktertreatment.setId(id);
            doktertreatment.setModified_By(1L);
            doktertreatment.setModified_On(new Date());
            this.dokterTreatmentRepo.save(doktertreatment);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletedoktertreatment/{id}")
    public String DeleteDokterTreatment(@PathVariable("id") Long id)
    {
        try {
            DokterTreatment doktertreatment = this.dokterTreatmentRepo.findByIdData(id);
            doktertreatment.setDeleted_By(1L);
            doktertreatment.setDeleted_On(new Date());
            doktertreatment.setIs_Delete(true);
            this.dokterTreatmentRepo.save(doktertreatment);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
