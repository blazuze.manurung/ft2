package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.Location;
import MiniProject.com.RumahSakitTeamA.models.LocationLevel;
import MiniProject.com.RumahSakitTeamA.repositories.LocationLevelRepo;
import MiniProject.com.RumahSakitTeamA.repositories.LocationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiLocationLevelController {

    @Autowired
    private LocationLevelRepo locationLevelRepo;
    @Autowired
    private LocationRepo locationRepo;


    @GetMapping("/getalllocationlevel")
    public ResponseEntity<List<LocationLevel>> GetAllLocationLevel()
    {
        try {
            List<LocationLevel> category = this.locationLevelRepo.findAllNotDeleted();
            return new ResponseEntity<>(category, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addlocationlevel")
    public ResponseEntity<Object> saveLocationLevel(@RequestBody LocationLevel locationLevel) {
        try {
            String namaLocation = locationLevel.getName();
            String abbreviation = locationLevel.getAbbreviation();

            // Validate that name and abbreviation are not empty
            if (namaLocation == null || namaLocation.trim().isEmpty() ||
                    abbreviation == null || abbreviation.trim().isEmpty()) {
                return new ResponseEntity<>("Nama dan Nama Singkat tidak boleh kosong!", HttpStatus.BAD_REQUEST);
            }

            // Validate duplicate name
            int existingNameCount = locationLevelRepo.countByNameIgnoreCaseAndNotDeleted(namaLocation);
            if (existingNameCount > 0) {
                // Get existing data with the same name
                List<LocationLevel> existingLocations = locationLevelRepo.findDuplicatesByNameIgnoreCaseAndNotDeleted(namaLocation);

                Map<String, Object> response = new HashMap<>();
                response.put("error", "Nama Level Lokasi sudah ada!");
                response.put("existingData", existingLocations);

                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }

            // Validate duplicate abbreviation
            int existingAbbreviationCount = locationLevelRepo.countByAbbreviationIgnoreCaseAndNotDeleted(abbreviation);
            if (existingAbbreviationCount > 0) {
                // Get existing data with the same abbreviation
                List<LocationLevel> existingLocations = locationLevelRepo.findDuplicatesByAbbreviationIgnoreCaseAndNotDeleted(abbreviation);

                Map<String, Object> response = new HashMap<>();
                response.put("error", "Nama Singkat Level Lokasi sudah ada!");
                response.put("existingData", existingLocations);

                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }

            // Save the new location level if no duplicates found
            locationLevel.setCreated_By(1L);
            locationLevel.setCreated_On(new Date());
            this.locationLevelRepo.save(locationLevel);
            return new ResponseEntity<>(locationLevel, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }



    @GetMapping("/getbyidlocationlevel/{id}")
    public ResponseEntity<List<LocationLevel>> GetLocationLevelById(@PathVariable("id") Long id)
    {
        try {
            Optional<LocationLevel> locationLevel = this.locationLevelRepo.findById(id);

            if (locationLevel.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(locationLevel, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/editlocationlevel/{id}")
    public ResponseEntity<Object> editLocationLevel(@RequestBody LocationLevel updatedLocationLevel, @PathVariable("id") Long id) {
        Optional<LocationLevel> existingLocationLevelOptional = this.locationLevelRepo.findById(id);

        if (existingLocationLevelOptional.isPresent()) {
            LocationLevel existingLocationLevel = existingLocationLevelOptional.get();

            // Check if the updated name is different from the existing one
            if (!updatedLocationLevel.getName().equalsIgnoreCase(existingLocationLevel.getName())) {
                int existingNameCount = locationLevelRepo.countByNameIgnoreCaseAndNotDeletedAndIdNot(updatedLocationLevel.getName(), id);
                if (existingNameCount > 0) {
                    return new ResponseEntity<>("Nama Level Lokasi sudah ada!", HttpStatus.BAD_REQUEST);
                }
            }

            // Check if the updated abbreviation is different from the existing one
            if (!updatedLocationLevel.getAbbreviation().equalsIgnoreCase(existingLocationLevel.getAbbreviation())) {
                int existingAbbreviationCount = locationLevelRepo.countByAbbreviationIgnoreCaseAndNotDeletedAndIdNot(updatedLocationLevel.getAbbreviation(), id);
                if (existingAbbreviationCount > 0) {
                    return new ResponseEntity<>("Nama Singkat Level Lokasi sudah ada!", HttpStatus.BAD_REQUEST);
                }
            }

            // Update the modified details and save
            existingLocationLevel.setName(updatedLocationLevel.getName());
            existingLocationLevel.setAbbreviation(updatedLocationLevel.getAbbreviation());
            existingLocationLevel.setModified_By(1L);
            existingLocationLevel.setModified_On(new Date());

            this.locationLevelRepo.save(existingLocationLevel);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @DeleteMapping("/deletelocationlevel/{id}")
    public ResponseEntity<String> deleteLocationLevel(@PathVariable("id") Long id) {
        try {
            LocationLevel locationLevel = this.locationLevelRepo.findByIdData(id);
            // Periksa apakah level lokasi ini masih digunakan di lokasi
            List<Location> locationsUsingLevel = locationRepo.findByLocationLevelId(id);

            if (!locationsUsingLevel.isEmpty()) {
                return ResponseEntity.badRequest().body("Tidak dapat menghapus level lokasi ini karena masih digunakan di lokasi lain.");
            }

            // Lakukan penghapusan jika tidak ada lokasi yang menggunakan level lokasi ini
            locationLevel.setDeleted_By(1L);
            locationLevel.setIs_Delete(true);
            locationLevel.setDeleted_On(new Date());
            this.locationLevelRepo.save(locationLevel);

            return ResponseEntity.ok("Data level lokasi berhasil dihapus");
        } catch (Exception exception) {
            return ResponseEntity.notFound().build();
        }
    }




    @GetMapping("locationlevelmapped")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5")int size)
    {
        try{
            List <LocationLevel> locationlevel = this.locationLevelRepo.findAllNotDeleted();

            int start = page * size;
            int end = Math.min((start + size), locationlevel.size());

            List<LocationLevel> paginatedLocationLevel = locationlevel.subList(start, end);

            Map<String, Object> response = new HashMap<>();
            response.put("locationlevel", paginatedLocationLevel);
            response.put("currentPage", page);
            response.put("totalItems", locationlevel.size());
            response.put("totalPages",(int)Math.ceil((double) locationlevel.size() / size));

            return new ResponseEntity<>(response,HttpStatus.OK);
        }

        catch (Exception exception){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/searchlocationlevel/{keyword}")
    public ResponseEntity<List<LocationLevel>> SearchLocationLevelName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<LocationLevel> locationLevel = this.locationLevelRepo.SearchLocationLevel(keyword);
            return new ResponseEntity<>(locationLevel, HttpStatus.OK);
        } else {
            List<LocationLevel> locationLevel = this.locationLevelRepo.findAllNotDeleted();
            return new ResponseEntity<>(locationLevel, HttpStatus.OK);
        }
    }
}


//package MiniProject.com.RumahSakitTeamA.controllers;
//
//import MiniProject.com.RumahSakitTeamA.models.HubunganPasien;
//import MiniProject.com.RumahSakitTeamA.models.Location;
//import MiniProject.com.RumahSakitTeamA.models.LocationLevel;
//import MiniProject.com.RumahSakitTeamA.repositories.HubunganPasienRepo;
//import MiniProject.com.RumahSakitTeamA.repositories.LocationLevelRepo;
//import MiniProject.com.RumahSakitTeamA.repositories.LocationRepo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.*;
//
//@RestController
//@CrossOrigin("*")
//@RequestMapping("/api")
//public class ApiLocationLevelController {
//
//    @Autowired
//    private LocationLevelRepo locationLevelRepo;
//    @Autowired
//    private LocationRepo locationRepo;
//
//
//    @GetMapping("/getalllocationlevel")
//    public ResponseEntity<List<LocationLevel>> GetAllLocationLevel()
//    {
//        try {
//            List<LocationLevel> category = this.locationLevelRepo.findAllNotDeleted();
//            return new ResponseEntity<>(category, HttpStatus.OK);
//        }
//
//        catch (Exception exception) {
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//    }
//
//    @PostMapping("/addlocationlevel")
//    public ResponseEntity<Object> saveLocationLevel(@RequestBody LocationLevel locationLevel) {
//        try {
//            String namaLocation = locationLevel.getName();
//            String abbreviation = locationLevel.getAbbreviation();
//
//            // Validate that name and abbreviation are not empty
//            if (namaLocation == null || namaLocation.trim().isEmpty() ||
//                    abbreviation == null || abbreviation.trim().isEmpty()) {
//                return new ResponseEntity<>("Nama dan Nama Singkat tidak boleh kosong!", HttpStatus.BAD_REQUEST);
//            }
//
//            // Validate duplicate name
//            int existingNameCount = locationLevelRepo.countByNameIgnoreCaseAndNotDeleted(namaLocation);
//            if (existingNameCount > 0) {
//                // Get existing data with the same name
//                List<LocationLevel> existingLocations = locationLevelRepo.findDuplicatesByNameIgnoreCaseAndNotDeleted(namaLocation);
//
//                Map<String, Object> response = new HashMap<>();
//                response.put("error", "Nama Level Lokasi sudah ada!");
//                response.put("existingData", existingLocations);
//
//                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
//            }
//
//            // Validate duplicate abbreviation
//            int existingAbbreviationCount = locationLevelRepo.countByAbbreviationIgnoreCaseAndNotDeleted(abbreviation);
//            if (existingAbbreviationCount > 0) {
//                // Get existing data with the same abbreviation
//                List<LocationLevel> existingLocations = locationLevelRepo.findDuplicatesByAbbreviationIgnoreCaseAndNotDeleted(abbreviation);
//
//                Map<String, Object> response = new HashMap<>();
//                response.put("error", "Nama Singkat Level Lokasi sudah ada!");
//                response.put("existingData", existingLocations);
//
//                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
//            }
//
//            // Save the new location level if no duplicates found
//            locationLevel.setCreated_By(1L);
//            locationLevel.setCreated_On(new Date());
//            this.locationLevelRepo.save(locationLevel);
//            return new ResponseEntity<>(locationLevel, HttpStatus.OK);
//        } catch (Exception exception) {
//            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
//        }
//    }
//
//
//
//    @GetMapping("/getbyidlocationlevel/{id}")
//    public ResponseEntity<List<LocationLevel>> GetLocationLevelById(@PathVariable("id") Long id)
//    {
//        try {
//            Optional<LocationLevel> locationLevel = this.locationLevelRepo.findById(id);
//
//            if (locationLevel.isPresent())
//            {
//                ResponseEntity rest = new ResponseEntity<>(locationLevel, HttpStatus.OK);
//                return rest;
//            } else {
//                return ResponseEntity.notFound().build();
//            }
//        }
//
//        catch (Exception exception) {
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//    }
//
//    @PutMapping("/editlocationlevel/{id}")
//    public ResponseEntity<Object> editLocationLevel(@RequestBody LocationLevel updatedLocationLevel, @PathVariable("id") Long id) {
//        Optional<LocationLevel> existingLocationLevelOptional = this.locationLevelRepo.findById(id);
//
//        if (existingLocationLevelOptional.isPresent()) {
//            LocationLevel existingLocationLevel = existingLocationLevelOptional.get();
//
//            // Check if the updated name is different from the existing one
//            if (!updatedLocationLevel.getName().equalsIgnoreCase(existingLocationLevel.getName())) {
//                int existingNameCount = locationLevelRepo.countByNameIgnoreCaseAndNotDeletedAndIdNot(updatedLocationLevel.getName(), id);
//                if (existingNameCount > 0) {
//                    return new ResponseEntity<>("Nama Level Lokasi sudah ada!", HttpStatus.BAD_REQUEST);
//                }
//            }
//
//            // Check if the updated abbreviation is different from the existing one
//            if (!updatedLocationLevel.getAbbreviation().equalsIgnoreCase(existingLocationLevel.getAbbreviation())) {
//                int existingAbbreviationCount = locationLevelRepo.countByAbbreviationIgnoreCaseAndNotDeletedAndIdNot(updatedLocationLevel.getAbbreviation(), id);
//                if (existingAbbreviationCount > 0) {
//                    return new ResponseEntity<>("Nama Singkat Level Lokasi sudah ada!", HttpStatus.BAD_REQUEST);
//                }
//            }
//
//            // Update the modified details and save
//            existingLocationLevel.setName(updatedLocationLevel.getName());
//            existingLocationLevel.setAbbreviation(updatedLocationLevel.getAbbreviation());
//            existingLocationLevel.setModified_By(1L);
//            existingLocationLevel.setModified_On(new Date());
//
//            this.locationLevelRepo.save(existingLocationLevel);
//            return new ResponseEntity<>("Success", HttpStatus.OK);
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }
//
//
//    @DeleteMapping("/deletelocationlevel/{id}")
//    public ResponseEntity<String> deleteLocationLevel(@PathVariable("id") Long id) {
//        try {
//            LocationLevel locationLevel = this.locationLevelRepo.findByIdData(id);
//            // Periksa apakah level lokasi ini masih digunakan di lokasi
//            List<Location> locationsUsingLevel = locationRepo.findByLocationLevelId(id);
//
//            if (!locationsUsingLevel.isEmpty()) {
//                return ResponseEntity.badRequest().body("Tidak dapat menghapus level lokasi ini karena masih digunakan di lokasi lain.");
//            }
//
//            // Lakukan penghapusan jika tidak ada lokasi yang menggunakan level lokasi ini
//            locationLevel.setDeleted_By(1L);
//            locationLevel.setIs_Delete(true);
//            locationLevel.setDeleted_On(new Date());
//            this.locationLevelRepo.save(locationLevel);
//
//            return ResponseEntity.ok("Data level lokasi berhasil dihapus");
//        } catch (Exception exception) {
//            return ResponseEntity.notFound().build();
//        }
//    }
//
//
//
//
//    @GetMapping("locationlevelmapped")
//    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5")int size)
//    {
//        try{
//            List <LocationLevel> locationlevel = this.locationLevelRepo.findAllNotDeleted();
//
//            int start = page * size;
//            int end = Math.min((start + size), locationlevel.size());
//
//            List<LocationLevel> paginatedLocationLevel = locationlevel.subList(start, end);
//
//            Map<String, Object> response = new HashMap<>();
//            response.put("locationlevel", paginatedLocationLevel);
//            response.put("currentPage", page);
//            response.put("totalItems", locationlevel.size());
//            response.put("totalPages",(int)Math.ceil((double) locationlevel.size() / size));
//
//            return new ResponseEntity<>(response,HttpStatus.OK);
//        }
//
//        catch (Exception exception){
//            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//    @GetMapping("/searchlocationlevel/{keyword}")
//    public ResponseEntity<List<LocationLevel>> SearchLocationLevelName(@PathVariable("keyword") String keyword)
//    {
//        if (keyword != null)
//        {
//            List<LocationLevel> locationLevel = this.locationLevelRepo.SearchLocationLevel(keyword);
//            return new ResponseEntity<>(locationLevel, HttpStatus.OK);
//        } else {
//            List<LocationLevel> locationLevel = this.locationLevelRepo.findAllNotDeleted();
//            return new ResponseEntity<>(locationLevel, HttpStatus.OK);
//        }
//    }
//}
//
//

