package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.M_Wallet_Default_Nominal;
import MiniProject.com.RumahSakitTeamA.repositories.NominalDompetElektronikRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiNominalDompetElektronikController {

    @Autowired
    private NominalDompetElektronikRepo nominalDompetElektronikRepo;

    @GetMapping("/getallnominaldompetelektronik")
    public ResponseEntity<List<M_Wallet_Default_Nominal>> GetAllNominalDompetElektronik()
    {
        try {
            List<M_Wallet_Default_Nominal> nominalDompetElektronik = this.nominalDompetElektronikRepo.findAllNotDeleted();
            return new ResponseEntity<>(nominalDompetElektronik, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addnominaldompetelektronik")
    public ResponseEntity<Object> SaveNominalDompetElektronik(@RequestBody M_Wallet_Default_Nominal nominalDompetElektronik)
    {
        try {
            nominalDompetElektronik.setCreated_By(1L);
            nominalDompetElektronik.setCreated_On(new Date());
            this.nominalDompetElektronikRepo.save(nominalDompetElektronik);
            return new ResponseEntity<>(nominalDompetElektronik, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getbyidnominaldompetelektronik/{id}")
    public ResponseEntity<List<M_Wallet_Default_Nominal>> GetNominalDompetElektronikById(@PathVariable("id") Long id)
    {
        try {
            Optional<M_Wallet_Default_Nominal> nominalDompetElektronik = this.nominalDompetElektronikRepo.findById(id);

            if (nominalDompetElektronik.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(nominalDompetElektronik, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/editnominaldompetelektronik/{id}")
    public ResponseEntity<Object> EditNominalDompetElektronik(@RequestBody M_Wallet_Default_Nominal nominalDompetElektronik, @PathVariable("id") Long id)
    {
        Optional<M_Wallet_Default_Nominal> nominalDompetElektronikData = this.nominalDompetElektronikRepo.findById(id);

        if (nominalDompetElektronikData.isPresent())
        {
            nominalDompetElektronik.setId(id);
            nominalDompetElektronik.setModified_By(1L);
            nominalDompetElektronik.setModified_On(new Date());
            this.nominalDompetElektronikRepo.save(nominalDompetElektronik);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deletenominaldompetelektronik/{id}")
    public String DeleteNominalDompetElektronik(@PathVariable("id") Long id)
    {
        try {
            M_Wallet_Default_Nominal nominalDompetElektronik = this.nominalDompetElektronikRepo.findByIdData(id);
            nominalDompetElektronik.setDeleted_By(1L);
            nominalDompetElektronik.setDeleted_On(new Date());
            nominalDompetElektronik.setIs_Delete(true);
            this.nominalDompetElektronikRepo.save(nominalDompetElektronik);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
    @GetMapping("/searchnominaldompetelektronik/{keyword}")
    public ResponseEntity<List<M_Wallet_Default_Nominal>> SearchNominalDompetElektronik(@PathVariable("keyword") Integer keyword)
    {
        if (keyword != null)
        {
            List<M_Wallet_Default_Nominal> nominalDompetElektronik = this.nominalDompetElektronikRepo.SearchNominalDompetElektronik(keyword);
            return new ResponseEntity<>(nominalDompetElektronik, HttpStatus.OK);
        } else {
            List<M_Wallet_Default_Nominal> nominalDompetElektronik = this.nominalDompetElektronikRepo.findAllNotDeleted();
            return new ResponseEntity<>(nominalDompetElektronik, HttpStatus.OK);
        }
    }
}
