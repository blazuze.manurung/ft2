<<<<<<< HEAD
//package MiniProject.com.RumahSakitTeamA.controllers;
//
//import MiniProject.com.RumahSakitTeamA.models.Location;
//import MiniProject.com.RumahSakitTeamA.models.LocationLevel;
//import MiniProject.com.RumahSakitTeamA.repositories.LocationLevelRepo;
//import MiniProject.com.RumahSakitTeamA.repositories.LocationRepo;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.data.domain.Pageable;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//
//import javax.persistence.NonUniqueResultException;
//import java.util.*;
//
//@RestController
//@CrossOrigin("*")
//@RequestMapping("/api")
//public class ApiLocationController {
//
//    @Autowired
//    private LocationRepo locationRepo;
//    @GetMapping("/getlocation")
//    public ResponseEntity<List<Location>> GetAlllocation() {
//        try {
//            List<Location> location = this.locationRepo.findAllNotDeleted();
//            return new ResponseEntity<>(location, HttpStatus.OK);
//        } catch (Exception exception) {
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//    }
//
//    @PostMapping("/addlocation")
//    public ResponseEntity<Object> Savelocation(@RequestBody Location location) {
//        try {
//            Location existingLocation = null;
//
//            if (location.getParentLocation() != null) {
//                existingLocation = locationRepo.findByNameAndParentIdAndLocationLevelId(
//                        location.getName(),
//                        location.getParentLocation().getId(),
//                        location.getLocationLevel().getId()
//                );
//            } else {
//                existingLocation = locationRepo.findByNameAndLocationLevelId(
//                        location.getName(),
//                        location.getLocationLevel().getId()
//                );
//            }
//
//            if (existingLocation != null) {
//                return new ResponseEntity<>("Data lokasi sudah ada.", HttpStatus.BAD_REQUEST);
//            }
//            location.setCreated_By(1L);
//            location.setCreated_On(new Date());
//            this.locationRepo.save(location);
//            return new ResponseEntity<>(location, HttpStatus.OK);
//        } catch (NonUniqueResultException e) {
//            String errorMessage = "Terjadi kesalahan: Nama lokasi atau level lokasi sudah di gunakan.";
//            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
//        } catch (Exception exception) {
//            String errorMessage = "Terjadi kesalahan: " + exception.getMessage();
//            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
//        }
//    }
//
//    @GetMapping("/location/{id}")
//    public ResponseEntity<List<Location>> GetlocationById(@PathVariable("id") Long id) {
//        try {
//            Optional<Location> location = this.locationRepo.findById(id);
//
//            if (location.isPresent()) {
//                ResponseEntity rest = new ResponseEntity<>(location, HttpStatus.OK);
//                return rest;
//            } else {
//                return ResponseEntity.notFound().build();
//            }
//        } catch (Exception exception) {
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//    }
//
//    @PutMapping("/location/{id}")
//    public ResponseEntity<Object> Updatelocation(@RequestBody Location location, @PathVariable("id") Long id) {
//        Optional<Location> locationOptional = this.locationRepo.findById(id);
//
//        if (locationOptional.isPresent()) {
//            Location existinglocation = locationOptional.get();
//
//            Date createOn = existinglocation.getCreated_On();
//            Long createdBy = existinglocation.getCreated_By();
//
//            if (createOn != null && createdBy != null) {
//                existinglocation.setName(location.getName());
//
//                existinglocation.setModified_By(1L);
//                existinglocation.setModified_On(new Date());
//
//                try {
//                    Location existingDuplicateLocation = null;
//
//                    if (location.getParentLocation() != null) {
//                        existingDuplicateLocation = locationRepo.findByNameAndParentIdAndLocationLevelIdAndIdNot(
//                                location.getName(),
//                                location.getParentLocation().getId(),
//                                location.getLocationLevel().getId(),
//                                id
//                        );
//                    } else {
//                        existingDuplicateLocation = locationRepo.findByNameAndLocationLevelIdAndIdNot(
//                                location.getName(),
//                                location.getLocationLevel().getId(),
//                                id
//                        );
//                    }
//
//                    if (existingDuplicateLocation != null) {
//                        return new ResponseEntity<>("Data lokasi sudah ada.", HttpStatus.BAD_REQUEST);
//                    }
//
//                    this.locationRepo.save(existinglocation);
//                    ResponseEntity<Object> response = new ResponseEntity<>("Success", HttpStatus.OK);
//                    return response;
//                } catch (Exception e) {
//                    return ResponseEntity.badRequest().body("Terjadi kesalahan: " + e.getMessage());
//                }
//            } else {
//                return ResponseEntity.badRequest().body("Nilai createOn atau createdBy kosong");
//            }
//        } else {
//            return ResponseEntity.notFound().build();
//        }
//    }
//
//
//    @GetMapping("/searchlocation/{keyword}")
//    public ResponseEntity<List<Location>> Searchlocation(@PathVariable("keyword") String keyword) {
//        if (keyword != null) {
//            List<Location> locations = this.locationRepo.SearchLocation(keyword);
//            return new ResponseEntity<>(locations, HttpStatus.OK);
//        } else {
//            List<Location> locations = this.locationRepo.findAll();
//            return new ResponseEntity<>(locations, HttpStatus.OK);
//        }
//    }
//
//    @GetMapping("/locationmapped")
//    public ResponseEntity<Map<String, Object>> GetPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {
//        try {
//            List<Location> locations = this.locationRepo.findAllNotDeleted();
//
//            int start = page * size;
//            int end = Math.min((start + size), locations.size());
//            List<Location> paginatedlocation = locations.subList(start, end);
//
//            Map<String, Object> response = new HashMap<>();
//            response.put("location", paginatedlocation);
//            response.put("currentPage", page);
//            response.put("totalItems", locations.size());
//            response.put("totalPages", (int) Math.ceil((double) locations.size() / size));
//
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//
//    @DeleteMapping("/location/{id}")
//    public ResponseEntity<String> deleteLocation(@PathVariable("id") Long id) {
//        try {
//            Location location = this.locationRepo.findByIdData(id);
//
//            // Periksa apakah lokasi ini masih digunakan sebagai parentLocation pada lokasi lain
//            List<Location> childLocations = locationRepo.findByParentLocation(location);
//
//            if (!childLocations.isEmpty()) {
//                return ResponseEntity.badRequest().body("Tidak dapat menghapus lokasi ini karena masih digunakan");
//            }
//
//            // Lakukan penghapusan jika tidak ada lokasi lain yang menggunakan lokasi ini sebagai parentLocation
//            location.setDeleted_By(1L);
//            location.setIs_Delete(true);
//            location.setDeleted_On(new Date());
//            this.locationRepo.save(location);
//
//
//            return ResponseEntity.ok("Data lokasi berhasil dihapus");
//        } catch (Exception exception) {
//            return ResponseEntity.notFound().build();
//        }
//    }
//    @GetMapping("/location/byLevel/{levelId}")
//    public ResponseEntity<List<Location>> GetLocationsByLevelId(@PathVariable("levelId") Long levelId) {
//        try {
//            List<Location> locations = this.locationRepo.findByLocationLevelId(levelId);
//            return new ResponseEntity<>(locations, HttpStatus.OK);
//        } catch (Exception exception) {
//            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//        }
//    }
//}
=======
package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.Location;
import MiniProject.com.RumahSakitTeamA.models.LocationLevel;
import MiniProject.com.RumahSakitTeamA.repositories.LocationLevelRepo;
import MiniProject.com.RumahSakitTeamA.repositories.LocationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.persistence.NonUniqueResultException;
import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiLocationController {

    @Autowired
    private LocationRepo locationRepo;
    @GetMapping("/getlocation")
    public ResponseEntity<List<Location>> GetAlllocation() {
        try {
            List<Location> location = this.locationRepo.findAllNotDeleted();
            return new ResponseEntity<>(location, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyidlocation/{id}")
    public ResponseEntity<List<Location>>GetLocationById(@PathVariable("id") Long id)
    {
        try {
            Optional<Location> location = this.locationRepo.findById(id);

            if(location.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(location, HttpStatus.OK);
                return rest;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

//    @GetMapping("/searchLocation/{keyword}")
//    public ResponseEntity<List<Location>> SearchKotaName(@PathVariable("keyword") String keyword)
//    {
//        if (keyword != null)
//        {
//            List<Location> location = this.locationRepo.SearchLocation(keyword);
//            return new ResponseEntity<>(location, HttpStatus.OK);
//        } else {
//            List<Location> location = this.locationRepo.findAll();
//            return new ResponseEntity<>(location, HttpStatus.OK);
//        }
//    }
//    @GetMapping("/locationMapped")
//    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
//    {
//        try {
//            List<Location> location = new ArrayList<>();
//            Pageable pagingSort = PageRequest.of(page, size);
//
//            Page<Location> pageTuts;
//
//            pageTuts = locationRepo.FindAllLocationOrderByASC(pagingSort);
//
//            location = pageTuts.getContent();
//
//            Map<String, Object> response = new HashMap<>();
//            response.put("location", location);
//            response.put("currentPage", pageTuts.getNumber());
//            response.put("totalItems", pageTuts.getTotalElements());
//            response.put("totalPages", pageTuts.getTotalPages());
//
//            return new ResponseEntity<>(response, HttpStatus.OK);
//        }
//
//        catch (Exception e)
//        {
//            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }

    @PostMapping("/addlocation")
    public ResponseEntity<Object> Savelocation(@RequestBody Location location) {
        try {
            Location existingLocation = null;

            if (location.getParentLocation() != null) {
                existingLocation = locationRepo.findByNameAndParentIdAndLocationLevelId(
                        location.getName(),
                        location.getParentLocation().getId(),
                        location.getLocationLevel().getId()
                );
            } else {
                existingLocation = locationRepo.findByNameAndLocationLevelId(
                        location.getName(),
                        location.getLocationLevel().getId()
                );
            }

            if (existingLocation != null) {
                return new ResponseEntity<>("Data lokasi sudah ada.", HttpStatus.BAD_REQUEST);
            }
            location.setCreated_By(1L);
            location.setCreated_On(new Date());
            this.locationRepo.save(location);
            return new ResponseEntity<>(location, HttpStatus.OK);
        } catch (NonUniqueResultException e) {
            String errorMessage = "Terjadi kesalahan: Nama lokasi atau level lokasi sudah di gunakan.";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        } catch (Exception exception) {
            String errorMessage = "Terjadi kesalahan: " + exception.getMessage();
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/location/{id}")
    public ResponseEntity<List<Location>> GetlocationById(@PathVariable("id") Long id) {
        try {
            Optional<Location> location = this.locationRepo.findById(id);

            if (location.isPresent()) {
                ResponseEntity rest = new ResponseEntity<>(location, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/location/{id}")
    public ResponseEntity<Object> Updatelocation(@RequestBody Location location, @PathVariable("id") Long id) {
        Optional<Location> locationOptional = this.locationRepo.findById(id);

        if (locationOptional.isPresent()) {
            Location existinglocation = locationOptional.get();

            Date createOn = existinglocation.getCreated_On();
            Long createdBy = existinglocation.getCreated_By();

            if (createOn != null && createdBy != null) {
                existinglocation.setName(location.getName());

                existinglocation.setModified_By(1L);
                existinglocation.setModified_On(new Date());

                try {
                    Location existingDuplicateLocation = null;

                    if (location.getParentLocation() != null) {
                        existingDuplicateLocation = locationRepo.findByNameAndParentIdAndLocationLevelIdAndIdNot(
                                location.getName(),
                                location.getParentLocation().getId(),
                                location.getLocationLevel().getId(),
                                id
                        );
                    } else {
                        existingDuplicateLocation = locationRepo.findByNameAndLocationLevelIdAndIdNot(
                                location.getName(),
                                location.getLocationLevel().getId(),
                                id
                        );
                    }

                    if (existingDuplicateLocation != null) {
                        return new ResponseEntity<>("Data lokasi sudah ada.", HttpStatus.BAD_REQUEST);
                    }

                    this.locationRepo.save(existinglocation);
                    ResponseEntity<Object> response = new ResponseEntity<>("Success", HttpStatus.OK);
                    return response;
                } catch (Exception e) {
                    return ResponseEntity.badRequest().body("Terjadi kesalahan: " + e.getMessage());
                }
            } else {
                return ResponseEntity.badRequest().body("Nilai createOn atau createdBy kosong");
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @GetMapping("/searchlocation/{keyword}")
    public ResponseEntity<List<Location>> Searchlocation(@PathVariable("keyword") String keyword) {
        if (keyword != null) {
            List<Location> locations = this.locationRepo.SearchLocation(keyword);
            return new ResponseEntity<>(locations, HttpStatus.OK);
        } else {
            List<Location> locations = this.locationRepo.findAll();
            return new ResponseEntity<>(locations, HttpStatus.OK);
        }
    }

    @GetMapping("/locationmapped")
    public ResponseEntity<Map<String, Object>> GetPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {
        try {
            List<Location> locations = this.locationRepo.findAllNotDeleted();

            int start = page * size;
            int end = Math.min((start + size), locations.size());
            List<Location> paginatedlocation = locations.subList(start, end);

            Map<String, Object> response = new HashMap<>();
            response.put("location", paginatedlocation);
            response.put("currentPage", page);
            response.put("totalItems", locations.size());
            response.put("totalPages", (int) Math.ceil((double) locations.size() / size));

            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/location/{id}")
    public ResponseEntity<String> deleteLocation(@PathVariable("id") Long id) {
        try {
            Location location = this.locationRepo.findByIdData(id);

            // Periksa apakah lokasi ini masih digunakan sebagai parentLocation pada lokasi lain
            List<Location> childLocations = locationRepo.findByParentLocation(location);

            if (!childLocations.isEmpty()) {
                return ResponseEntity.badRequest().body("Tidak dapat menghapus lokasi ini karena masih digunakan");
            }

            // Lakukan penghapusan jika tidak ada lokasi lain yang menggunakan lokasi ini sebagai parentLocation
            location.setDeleted_By(1L);
            location.setIs_Delete(true);
            location.setDeleted_On(new Date());
            this.locationRepo.save(location);


            return ResponseEntity.ok("Data lokasi berhasil dihapus");
        } catch (Exception exception) {
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/location/byLevel/{levelId}")
    public ResponseEntity<List<Location>> GetLocationsByLevelId(@PathVariable("levelId") Long levelId) {
        try {
            List<Location> locations = this.locationRepo.findByLocationLevelId(levelId);
            return new ResponseEntity<>(locations, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
>>>>>>> origin/main
