package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.Admin;
import MiniProject.com.RumahSakitTeamA.repositories.AdminRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiAdmin {

    @Autowired
    private AdminRepo adminRepo;

    @GetMapping("/admin/getAllAdmin")
    public ResponseEntity<List<Admin>> getAllAdmin(){
        try {
            List<Admin> m_admin = this.adminRepo.findAll();
            return new ResponseEntity<>(m_admin, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/admin")
    public ResponseEntity<Object> saveAdmin(@RequestBody Admin m_admin){
        try {
            m_admin.setCreated_By(1L);
            m_admin.setCreated_On(new Date());
            this.adminRepo.save(m_admin);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
