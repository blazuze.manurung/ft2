package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.models.DoctorOffice;
import MiniProject.com.RumahSakitTeamA.repositories.BiodataRepo;
import MiniProject.com.RumahSakitTeamA.repositories.DoctorOfficeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiDoctorOfficeController {

    @Autowired
    private DoctorOfficeRepo doctorOfficeRepo;

    @GetMapping("/getalldoctoroffice")
    public ResponseEntity<List<DoctorOffice>> GetAlldoctoroffice()
    {
        try {
            List<DoctorOffice> doctoroffice = this.doctorOfficeRepo.findAll();
            return new ResponseEntity<>(doctoroffice, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyiddoctoroffice/{id}")
    public ResponseEntity<List<DoctorOffice>> GetDoctorOfficeById(@PathVariable("id") Long id)
    {
        try {
            Optional<DoctorOffice> doctoroffice = this.doctorOfficeRepo.findById(id);

            if (doctoroffice.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(doctoroffice, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/adddoctoroffice")
    public ResponseEntity<Object> SaveDoctorOffice(@RequestBody DoctorOffice doctoroffice)
    {
        try {
            doctoroffice.setCreated_By(1L);
            doctoroffice.setCreated_On(new Date());
            this.doctorOfficeRepo.save(doctoroffice);
            return new ResponseEntity<>(doctoroffice, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("editdoctoroffice/{id}")
    public ResponseEntity<Object> EditDoctorOffice(@RequestBody DoctorOffice doctoroffice, @PathVariable("id") Long id)
    {
        Optional<DoctorOffice> doctorofficeData = this.doctorOfficeRepo.findById(id);

        if (doctorofficeData.isPresent())
        {
            doctoroffice.setId(id);
            doctoroffice.setModified_By(1L);
            doctoroffice.setModified_On(new Date());
            this.doctorOfficeRepo.save(doctoroffice);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletedoctoroffice/{id}")
    public String DeleteDoctorOffice(@PathVariable("id") Long id)
    {
        try {
            DoctorOffice doctoroffice = this.doctorOfficeRepo.findByIdData(id);
            doctoroffice.setDeleted_By(1L);
            doctoroffice.setDeleted_On(new Date());
            doctoroffice.setIs_Delete(true);
            this.doctorOfficeRepo.save(doctoroffice);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
