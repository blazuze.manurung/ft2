package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.SpesialisasiDokter;
import MiniProject.com.RumahSakitTeamA.repositories.SpesialisasiDokterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiSpesialisasiDokterController {
    @Autowired
    private SpesialisasiDokterRepo spesialisasiDokterRepo;

    @GetMapping("/getallspesialisasidokter")
    public ResponseEntity<List<SpesialisasiDokter>> GetAllSpesialisasiDokter()
    {
        try {
            List<SpesialisasiDokter> spesialisasidokter = this.spesialisasiDokterRepo.FindAllSpesialisasiDokterOrderByASC();
            return new ResponseEntity<>(spesialisasidokter, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyidspesialisasidokter/{id}")
    public ResponseEntity<List<SpesialisasiDokter>>GetSpesialisasiDokterById(@PathVariable("id") Long id)
    {
        try {
            Optional<SpesialisasiDokter> spesialisasidokter = this.spesialisasiDokterRepo.findById(id);

            if(spesialisasidokter.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(spesialisasidokter, HttpStatus.OK);
                return rest;
            }else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addspesialisasidokter")
    public ResponseEntity<Object> SaveSpesialisasiDokter(@RequestBody SpesialisasiDokter spesialisasidokter)
    {
        try {
            spesialisasidokter.setCreated_By(1L);
            spesialisasidokter.setCreated_On(new Date());
            this.spesialisasiDokterRepo.save(spesialisasidokter);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/editspesialisasidokter/{id}")
    public ResponseEntity<Object> UpdateSpesialisasiDokter(@RequestBody SpesialisasiDokter spesialisasidokter, @PathVariable("id") Long id)
    {
        Optional<SpesialisasiDokter> spesialisasidokterData = this.spesialisasiDokterRepo.findById(id);

        if (spesialisasidokterData.isPresent()){
            spesialisasidokter.setModified_By(1L);
            spesialisasidokter.setModified_On(new Date());
            spesialisasidokter.setId(id);
            this.spesialisasiDokterRepo.save(spesialisasidokter);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deletespesialisasidokter/{id}")
    public String DeleteSpesialisasiDokter(@PathVariable("id") Long id)
    {
        try {
            SpesialisasiDokter spesialisasiDokter = this.spesialisasiDokterRepo.findByIdData(id);
            spesialisasiDokter.setDeleted_By(1L);
            spesialisasiDokter.setDeleted_On(new Date());
            spesialisasiDokter.setIs_Delete(true);
            this.spesialisasiDokterRepo.save(spesialisasiDokter);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

    @GetMapping("/searchspesialisasidokter/{keyword}")
    public ResponseEntity<List<SpesialisasiDokter>> SearchSpesialisasiDokterName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<SpesialisasiDokter> spesialisasidokter = this.spesialisasiDokterRepo.SearchSpesialisasiDokter(keyword);
            return new ResponseEntity<>(spesialisasidokter, HttpStatus.OK);
        } else {
            List<SpesialisasiDokter> spesialisasidokter = this.spesialisasiDokterRepo.findAll();
            return new ResponseEntity<>(spesialisasidokter, HttpStatus.OK);
        }
    }

    @GetMapping("/mappedspesialisasidokter")
    public ResponseEntity<Map<String, Object>> GetAllPage(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size)
    {
        try {
            List<SpesialisasiDokter> spesialisasidokter = new ArrayList<>();
            Pageable pagingSort = PageRequest.of(page, size);

            Page<SpesialisasiDokter> pageTuts;

            pageTuts = spesialisasiDokterRepo.FindAllSpesialisasiDokterOrderByASC(pagingSort);

            spesialisasidokter = pageTuts.getContent();

            Map<String, Object> response = new HashMap<>();
            response.put("spesialisasidokter", spesialisasidokter);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());

            return new ResponseEntity<>(response, HttpStatus.OK);
        }

        catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
