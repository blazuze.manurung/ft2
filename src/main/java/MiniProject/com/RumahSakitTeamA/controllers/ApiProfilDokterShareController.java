package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.models.ProfilDokterShare;
import MiniProject.com.RumahSakitTeamA.repositories.BiodataRepo;
import MiniProject.com.RumahSakitTeamA.repositories.ProfilDokterShareRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiProfilDokterShareController {
    @Autowired
    private ProfilDokterShareRepo profilDokterShareRepo;

    @GetMapping("/getallprofildoktershare")
    public ResponseEntity<List<ProfilDokterShare>> GetAllProfilDokterShare()
    {
        try {
            List<ProfilDokterShare> profildoktershare = this.profilDokterShareRepo.findAll();
            return new ResponseEntity<>(profildoktershare, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyidprofildoktershare/{id}")
    public ResponseEntity<List<ProfilDokterShare>> GetProfilDokterShareById(@PathVariable("id") Long id)
    {
        try {
            List<ProfilDokterShare> profildoktershare = this.profilDokterShareRepo.findByIdDoctor(id);
            return new ResponseEntity<>(profildoktershare, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/profildoktershare/byHeader={id}")
    public ResponseEntity<List<ProfilDokterShare>> getRentalDetailByHeader(@PathVariable("id")Long id){
        try {
            List<ProfilDokterShare> profildoktershare = this.profilDokterShareRepo.findByIdProfil(id);
            return new ResponseEntity<>(profildoktershare, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }




    @PostMapping("/addprofildoktershare")
    public ResponseEntity<Object> SaveProfilDokterShare(@RequestBody ProfilDokterShare profildoktershare)
    {
        try {
            profildoktershare.setCreated_By(1L);
            profildoktershare.setCreated_On(new Date());
            this.profilDokterShareRepo.save(profildoktershare);
            return new ResponseEntity<>(profildoktershare, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("editprofildoktershare/{id}")
    public ResponseEntity<Object> EditProfilDokterShare(@RequestBody ProfilDokterShare profildoktershare, @PathVariable("id") Long id)
    {
        Optional<ProfilDokterShare> profilDokterShareData = this.profilDokterShareRepo.findById(id);

        if (profilDokterShareData.isPresent())
        {
            profildoktershare.setId(id);
            profildoktershare.setModified_By(1L);
            profildoktershare.setModified_On(new Date());
            this.profilDokterShareRepo.save(profildoktershare);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deleteprofildoktershare/{id}")
    public String DeleteProfilDokterShare(@PathVariable("id") Long id)
    {
        try {
            ProfilDokterShare profildoktershare = this.profilDokterShareRepo.findByIdData(id);
            profildoktershare.setDeleted_By(1L);
            profildoktershare.setDeleted_On(new Date());
            profildoktershare.setIs_Delete(true);
            this.profilDokterShareRepo.save(profildoktershare);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
