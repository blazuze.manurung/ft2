package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.M_Customer;
import MiniProject.com.RumahSakitTeamA.repositories.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiCustomerController {

    @Autowired
    private CustomerRepo customerRepo;

    @GetMapping("/getallcustomer")
    public ResponseEntity<List<M_Customer>> GetAllCustomer()
    {
        try {
            List<M_Customer> customer = this.customerRepo.findAllNotDeleted();
            return new ResponseEntity<>(customer, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addcustomer")
    public ResponseEntity<Object> SaveCustomer(@RequestBody M_Customer customer)
    {
        try {
            customer.setCreated_By(1L);
            customer.setCreated_On(new Date());
            this.customerRepo.save(customer);
            return new ResponseEntity<>(customer, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getbyidcustomer/{id}")
    public ResponseEntity<List<M_Customer>> GetCustomerById(@PathVariable("id") Long id)
    {
        try {
            Optional<M_Customer> customer = this.customerRepo.findById(id);

            if (customer.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(customer, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/editcustomer/{id}")
    public ResponseEntity<Object> EditCustomer(@RequestBody M_Customer customer, @PathVariable("id") Long id)
    {
        Optional<M_Customer> customerData = this.customerRepo.findById(id);

        if (customerData.isPresent())
        {
            customer.setId(id);
            customer.setModified_By(1L);
            customer.setModified_On(new Date());
            this.customerRepo.save(customer);
            ResponseEntity rest =new ResponseEntity<>(customer, HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deletecustomer/{id}")
    public String DeleteCustomer(@PathVariable("id") Long id)
    {
        try {
            M_Customer customer = this.customerRepo.findByIdData(id);
            customer.setDeleted_By(1L);
            customer.setDeleted_On(new Date());
            customer.setIs_Delete(true);
            this.customerRepo.save(customer);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
