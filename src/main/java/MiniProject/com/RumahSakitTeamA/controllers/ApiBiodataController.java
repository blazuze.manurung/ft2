package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.Biodata;
import MiniProject.com.RumahSakitTeamA.repositories.BiodataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiBiodataController {

    @Autowired
    private BiodataRepo biodataRepo;

    @GetMapping("/getallbiodata")
    public ResponseEntity<List<Biodata>> GetAllBiodata()
    {
        try {
            List<Biodata> biodata = this.biodataRepo.findAll();
            return new ResponseEntity<>(biodata, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyidbiodata/{id}")
    public ResponseEntity<List<Biodata>> GetBiodataById(@PathVariable("id") Long id)
    {
        try {
            Optional<Biodata> biodata = this.biodataRepo.findById(id);

            if (biodata.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(biodata, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addbiodata")
    public ResponseEntity<Object> SaveBiodata(@RequestBody Biodata biodata)
    {
        try {
            biodata.setCreated_By(1L);
            biodata.setCreated_On(new Date());
            this.biodataRepo.save(biodata);
            return new ResponseEntity<>(biodata, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    public static String downloadPath = "D:\\Image";
    @PostMapping("/postimage")
    public String upload(@RequestParam("file") MultipartFile file) {
        String fileOri = StringUtils.cleanPath(file.getOriginalFilename());
        Path path = Paths.get(downloadPath + fileOri);

        try {
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String downloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/image/").path(fileOri)
                .toUriString();
        return downloadUri;
    }

    @PutMapping("editbiodata/{id}")
    public ResponseEntity<Object> Editbiodata(@RequestBody Biodata biodata, @PathVariable("id") Long id)
    {
        Optional<Biodata> biodataData = this.biodataRepo.findById(id);

        if (biodataData.isPresent())
        {
            biodata.setId(id);
            biodata.setModified_By(1L);
            biodata.setModified_On(new Date());
            this.biodataRepo.save(biodata);
            ResponseEntity rest =new ResponseEntity<>(biodata, HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletebiodata/{id}")
    public String DeleteBiodata(@PathVariable("id") Long id)
    {
        try {
            Biodata biodata = this.biodataRepo.findByIdData(id);
            biodata.setDeleted_By(1L);
            biodata.setDeleted_On(new Date());
            biodata.setIs_Delete(true);
            this.biodataRepo.save(biodata);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
