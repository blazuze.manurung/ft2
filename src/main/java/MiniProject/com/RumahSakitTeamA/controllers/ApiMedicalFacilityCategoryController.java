package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.MedicalFacilityCategory;
import MiniProject.com.RumahSakitTeamA.repositories.MedicalFacilityCategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiMedicalFacilityCategoryController {
    @Autowired
    private MedicalFacilityCategoryRepo medicalFacilityCategoryRepo;

    @GetMapping("/getallmedicalfacilitycategory")
    public ResponseEntity<List<MedicalFacilityCategory>> GetAllMedicalFacilityCategory()
    {
        try {
            List<MedicalFacilityCategory> medicalfacilitycategory = this.medicalFacilityCategoryRepo.findAll();
            return new ResponseEntity<>(medicalfacilitycategory, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyidmedicalfacilitycategory/{id}")
    public ResponseEntity<List<MedicalFacilityCategory>> GetMedicalFacilityCategoryById(@PathVariable("id") Long id)
    {
        try {
            Optional<MedicalFacilityCategory> medicalfacilitycategory = this.medicalFacilityCategoryRepo.findById(id);

            if (medicalfacilitycategory.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(medicalfacilitycategory, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addmedicalfacilitycategory")
    public ResponseEntity<Object> SaveMedicalFacilityCategory(@RequestBody MedicalFacilityCategory medicalfacilitycategory)
    {
        try {
            medicalfacilitycategory.setCreated_By(1L);
            medicalfacilitycategory.setCreated_On(new Date());
            this.medicalFacilityCategoryRepo.save(medicalfacilitycategory);
            return new ResponseEntity<>(medicalfacilitycategory, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("editmedicalfacilitycategory/{id}")
    public ResponseEntity<Object> EditMedicalFacilityCategory(@RequestBody MedicalFacilityCategory medicalfacilitycategory, @PathVariable("id") Long id)
    {
        Optional<MedicalFacilityCategory> medicalfacilitycategoryData = this.medicalFacilityCategoryRepo.findById(id);

        if (medicalfacilitycategoryData.isPresent())
        {
            medicalfacilitycategory.setId(id);
            medicalfacilitycategory.setModified_By(1L);
            medicalfacilitycategory.setModified_On(new Date());
            this.medicalFacilityCategoryRepo.save(medicalfacilitycategory);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletemedicalfacilitycategory/{id}")
    public String Deletemedicalfacilitycategory(@PathVariable("id") Long id)
    {
        try {
            MedicalFacilityCategory medicalfacilitycategory = this.medicalFacilityCategoryRepo.findByIdData(id);
            medicalfacilitycategory.setDeleted_By(1L);
            medicalfacilitycategory.setDeleted_On(new Date());
            medicalfacilitycategory.setIs_Delete(true);
            this.medicalFacilityCategoryRepo.save(medicalfacilitycategory);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

}
