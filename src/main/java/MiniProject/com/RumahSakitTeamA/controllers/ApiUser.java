package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.User;
import MiniProject.com.RumahSakitTeamA.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiUser {
    @Autowired
    private UserRepo userRepo;

    @GetMapping("/user/session")
    public Map isSessionLogin(HttpSession httpSession){
        Long user_id = (Long) httpSession.getAttribute("userID");
        Long role_id = (Long) httpSession.getAttribute("roleID");

        if (user_id != null){
            Map obj = new HashMap();
            obj.put("user_id", user_id);
            obj.put("role_id", role_id);
            List data =  new ArrayList<>();
            data.add(obj);
            return obj;
        }else {
            Map obj = new HashMap();
            obj.put("user_id", 0);
            obj.put("role_id", 0);
            List data =  new ArrayList<>();
            data.add(obj);
            return obj;
        }
    }

    @GetMapping("/user/getAllUser")
    public ResponseEntity<List<User>> getAllUser(){
        try {
            List<User> m_user = this.userRepo.findAll();
            return new ResponseEntity<>(m_user, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/user/userID={user_id}")
    public ResponseEntity<Object> getUserById(@PathVariable("user_id") Long id){
        try {
            Optional<User> m_user = this.userRepo.findById(id);
            if (m_user.isPresent()) {
                return new ResponseEntity<>(m_user, HttpStatus.OK);
            } else {
                return ResponseEntity.notFound().build();
            }
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("user/email={email}")
    public boolean isEmailTerdaftar(@PathVariable("email") String email){
        Optional<User> m_user = this.userRepo.isEmailTerdaftar(email);
        if (m_user.isPresent()){
            return true;
        }else {
            return false;
        }
    }

    @PostMapping("/user/callResponse")
    public ResponseEntity<Object> saveUser(@RequestBody User m_user){
        try {
            m_user.setCreated_By(1L);
            m_user.setCreated_On(new Date());
            this.userRepo.save(m_user);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/user/login")
    public String sessionLogin(@RequestParam String email, @RequestParam String password, HttpSession session){
        User userLogin = this.userRepo.login(email, password);
        if (userLogin != null){
            Boolean islocked = userLogin.getIs_locked() == null ? false : userLogin.getIs_locked();
            if(!islocked){
                session.setAttribute("userID", userLogin.getId());
                session.setAttribute("roleID", userLogin.getRole_id());
                userLogin.setModified_By(userLogin.getId());
                userLogin.setModified_On(new Date());
                userLogin.setLast_login(new Date());
                this.userRepo.save(userLogin);
                return "sukses";
            }else {
                return "terkunci";
            }
        }else {
            Optional<User> cekEmail = this.userRepo.isEmailTerdaftar(email);
            if (cekEmail.isPresent()){
                Integer loginAttempt = cekEmail.get().getLogin_attempt() == null ? 1 : cekEmail.get().getLogin_attempt();
                if (loginAttempt >= 3){
                    User userLock = cekEmail.get();
                    userLock.setIs_locked(true);
                    this.userRepo.save(userLock);
                }else {
                    loginAttempt += 1;
                    cekEmail.get().setLogin_attempt(loginAttempt);
                    this.userRepo.save(cekEmail.get());
                }
                return "gagal";
            }else {
                return "no-email";
            }
        }
    }

    @PostMapping("/user/logout")
    public String seesionLogout(HttpSession session){
        session.invalidate();
        return "success";
    }
}
