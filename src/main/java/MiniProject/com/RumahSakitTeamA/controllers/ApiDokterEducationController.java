package MiniProject.com.RumahSakitTeamA.controllers;


import MiniProject.com.RumahSakitTeamA.models.DokterEducation;
import MiniProject.com.RumahSakitTeamA.repositories.DokterEducationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiDokterEducationController {
    @Autowired
    private DokterEducationRepo dokterEducationRepo;

    @GetMapping("/getalldoktereducation")
    public ResponseEntity<List<DokterEducation>> GetAlldoktereducation()
    {
        try {
            List<DokterEducation> doktereducation = this.dokterEducationRepo.findAll();
            return new ResponseEntity<>(doktereducation, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyiddoktereducation/{id}")
    public ResponseEntity<List<DokterEducation>> GetdoktereducationById(@PathVariable("id") Long id)
    {
        try {
            Optional<DokterEducation> doktereducation = this.dokterEducationRepo.findById(id);

            if (doktereducation.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(doktereducation, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/adddoktereducation")
    public ResponseEntity<Object> Savedoktereducation(@RequestBody DokterEducation doktereducation)
    {
        try {
            doktereducation.setCreated_By(1L);
            doktereducation.setCreated_On(new Date());
            this.dokterEducationRepo.save(doktereducation);
            return new ResponseEntity<>(doktereducation, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("editdoktereducation/{id}")
    public ResponseEntity<Object> Editdoktereducation(@RequestBody DokterEducation doktereducation, @PathVariable("id") Long id)
    {
        Optional<DokterEducation> doktereducationData = this.dokterEducationRepo.findById(id);

        if (doktereducationData.isPresent())
        {

            doktereducation.setModified_By(1L);
            doktereducation.setModified_On(new Date());
            this.dokterEducationRepo.save(doktereducation);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletedoktereducation/{id}")
    public String Deletedoktereducation(@PathVariable("id") Long id)
    {
        try {
            DokterEducation doktereducation = this.dokterEducationRepo.findByIdData(id);
            doktereducation.setDeleted_By(1L);
            doktereducation.setDeleted_On(new Date());
            doktereducation.setIs_Delete(true);
            this.dokterEducationRepo.save(doktereducation);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }

}
