package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.GolonganDarah;
import MiniProject.com.RumahSakitTeamA.repositories.GolonganDarahRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiGolonganDarahController {

    @Autowired
    public GolonganDarahRepo golonganDarahRepo;

    @GetMapping("/getallgolongandarah")
    public ResponseEntity<List<GolonganDarah>> getAllGolonganDarah(){
        try{
            List<GolonganDarah> golonganDarah = this.golonganDarahRepo.findAllNotDeleted();
            return new ResponseEntity<>(golonganDarah, HttpStatus.OK);
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addgolongandarah")
    public ResponseEntity<Object> addGolonganDarah(@RequestBody GolonganDarah golonganDarah)
    {
        try{
            golonganDarah.setCreated_By(1L);
            golonganDarah.setCreated_On(new Date());
            this.golonganDarahRepo.save(golonganDarah);
            return new ResponseEntity<>(golonganDarah, HttpStatus.OK);
        }
        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/getbyidgolongandarah/{id}")
    public ResponseEntity<List<GolonganDarah>> getGolonganDarahById(@PathVariable("id") Long id){
        try{
            Optional<GolonganDarah> golonganDarah = this.golonganDarahRepo.findById(id);

            if (golonganDarah.isPresent()){
                ResponseEntity rest = new ResponseEntity<>(golonganDarah,HttpStatus.OK);
                return rest;
            }
            else {
                return ResponseEntity.notFound().build();
            }
        }
        catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @PutMapping("/editgolongandarah/{id}")
    public ResponseEntity<Object> editGolonganDarah(@RequestBody GolonganDarah golonganDarah, @PathVariable("id") Long id){
        Optional<GolonganDarah> golonganDarahData = this.golonganDarahRepo.findById(id);

        if (golonganDarahData.isPresent()){
            golonganDarah.setId(id);
            golonganDarah.setModified_By(1L);
            golonganDarah.setModified_On(new Date());
            this.golonganDarahRepo.save(golonganDarah);
            ResponseEntity rest = new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/deletegolongandarah/{id}")
    public String deleteGolonganDarah(@PathVariable("id")Long id)
    {
        try {
            GolonganDarah golonganDarah = this.golonganDarahRepo.findByIdData(id);
            golonganDarah.setDeleted_By(1L);
            golonganDarah.setDeleted_On(new Date());
            golonganDarah.setIs_Delete(true);
            this.golonganDarahRepo.save(golonganDarah);
            return "OK";
        }
        catch (Exception exception){
            return "Data Tidak Ditemukan";
        }
    }
    @GetMapping("/searchgolongandarah/{keyword}")
    public ResponseEntity<List<GolonganDarah>> SearchGolonganDarahCode(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<GolonganDarah> golonganDarah = this.golonganDarahRepo.SearchGolonganDarah(keyword);
            return new ResponseEntity<>(golonganDarah, HttpStatus.OK);
        } else {
            List<GolonganDarah> golonganDarah = this.golonganDarahRepo.findAllNotDeleted();
            return new ResponseEntity<>(golonganDarah, HttpStatus.OK);
        }
    }
}
