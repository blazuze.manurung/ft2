package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.M_Customer_Relation;
import MiniProject.com.RumahSakitTeamA.repositories.HubunganPasienRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiHubunganPasienController {

    @Autowired
    private HubunganPasienRepo hubunganPasienRepo;

    @GetMapping("/getallhubunganpasien")
    public ResponseEntity<List<M_Customer_Relation>> GetAllHubunganPasien()
    {
        try {
            List<M_Customer_Relation> hubunganPasien = this.hubunganPasienRepo.findAllNotDeleted();
            return new ResponseEntity<>(hubunganPasien, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addhubunganpasien")
    public ResponseEntity<Object> SaveHubunganPasien(@RequestBody M_Customer_Relation hubunganPasien)
    {
        try {
            hubunganPasien.setCreated_By(1L);
            hubunganPasien.setCreated_On(new Date());
            this.hubunganPasienRepo.save(hubunganPasien);
            return new ResponseEntity<>(hubunganPasien, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getbyidhubunganpasien/{id}")
    public ResponseEntity<List<M_Customer_Relation>> GetHubunganPasienById(@PathVariable("id") Long id)
    {
        try {
            Optional<M_Customer_Relation> hubunganPasien = this.hubunganPasienRepo.findById(id);

            if (hubunganPasien.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(hubunganPasien, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/edithubunganpasien/{id}")
    public ResponseEntity<Object> EditHubunganPasien(@RequestBody M_Customer_Relation hubunganPasien, @PathVariable("id") Long id)
    {
        Optional<M_Customer_Relation> hubunganPasienData = this.hubunganPasienRepo.findById(id);

        if (hubunganPasienData.isPresent())
        {
            hubunganPasien.setId(id);
            hubunganPasien.setModified_By(1L);
            hubunganPasien.setModified_On(new Date());
            this.hubunganPasienRepo.save(hubunganPasien);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/deletehubunganpasien/{id}")
    public String DeleteHubunganPasien(@PathVariable("id") Long id)
    {
        try {
            M_Customer_Relation hubunganPasien = this.hubunganPasienRepo.findByIdData(id);
            hubunganPasien.setDeleted_By(1L);
            hubunganPasien.setDeleted_On(new Date());
            hubunganPasien.setIs_Delete(true);
            this.hubunganPasienRepo.save(hubunganPasien);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
    @GetMapping("/searchhubunganpasien/{keyword}")
    public ResponseEntity<List<M_Customer_Relation>> SearchHubunganPasienName(@PathVariable("keyword") String keyword)
    {
        if (keyword != null)
        {
            List<M_Customer_Relation> hubunganPasien = this.hubunganPasienRepo.SearchHubunganPasien(keyword);
            return new ResponseEntity<>(hubunganPasien, HttpStatus.OK);
        } else {
            List<M_Customer_Relation> hubunganPasien = this.hubunganPasienRepo.findAllNotDeleted();
            return new ResponseEntity<>(hubunganPasien, HttpStatus.OK);
        }
    }
}
