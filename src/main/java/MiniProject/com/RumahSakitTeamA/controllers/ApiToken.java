package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.TokenEmail;
import MiniProject.com.RumahSakitTeamA.repositories.TokenRepo;
import ognl.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiToken {

    @Autowired
    private TokenRepo tokenRepo;

    @GetMapping("/token/getAllToken")
    public ResponseEntity<List<TokenEmail>> getAllUser(){
        try {
            List<TokenEmail> t_token = this.tokenRepo.findAll();
            return new ResponseEntity<>(t_token, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping("/token/lasttoken/")
    public String getLastToken(@RequestParam String email, @RequestParam String used_for, @RequestParam String input){
        try {
            Optional<TokenEmail> t_token = this.tokenRepo.getLastToken(email, used_for,input);
            if (t_token.isPresent()){
                Long expired = t_token.get().getExpired_on().getTime() - new Date().getTime();
                Boolean is_expired = t_token.get().getIs_expired();
                    if (expired > 0 && !is_expired){
                        return "Bisa digunakan";
                    }else{
                        return "kaldaluarsa";
                    }
            }else{
                return "salah";
            }

        }catch (Exception exception){
            return exception.getMessage();
        }
    }
}
