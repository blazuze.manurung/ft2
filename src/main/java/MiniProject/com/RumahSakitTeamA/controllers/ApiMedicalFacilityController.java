package MiniProject.com.RumahSakitTeamA.controllers;

import MiniProject.com.RumahSakitTeamA.models.MedicalFacility;
import MiniProject.com.RumahSakitTeamA.repositories.MedicalFacilityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class ApiMedicalFacilityController {
    @Autowired
    private MedicalFacilityRepo medicalFacilityRepo;

    @GetMapping("/getallmedicalfacility")
    public ResponseEntity<List<MedicalFacility>> GetAllMedicalFacility()
    {
        try {
            List<MedicalFacility> medicalfacility = this.medicalFacilityRepo.findAll();
            return new ResponseEntity<>(medicalfacility, HttpStatus.OK);
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getbyidmedicalfacility/{id}")
    public ResponseEntity<List<MedicalFacility>> GetMedicalFacilityById(@PathVariable("id") Long id)
    {
        try {
            Optional<MedicalFacility> medicalfacility = this.medicalFacilityRepo.findById(id);

            if (medicalfacility.isPresent())
            {
                ResponseEntity rest = new ResponseEntity<>(medicalfacility, HttpStatus.OK);
                return rest;
            } else {
                return ResponseEntity.notFound().build();
            }
        }

        catch (Exception exception) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/addmedicalfacility")
    public ResponseEntity<Object> SaveMedicalFacility(@RequestBody MedicalFacility medicalfacility)
    {
        try {
            medicalfacility.setCreated_By(1L);
            medicalfacility.setCreated_On(new Date());
            this.medicalFacilityRepo.save(medicalfacility);
            return new ResponseEntity<>(medicalfacility, HttpStatus.OK);
        }

        catch (Exception exception)
        {
            return new ResponseEntity<>("Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("editmedicalfacility/{id}")
    public ResponseEntity<Object> EditMedicalFacility(@RequestBody MedicalFacility medicalfacility, @PathVariable("id") Long id)
    {
        Optional<MedicalFacility> medicalfacilityData = this.medicalFacilityRepo.findById(id);

        if (medicalfacilityData.isPresent())
        {
            medicalfacility.setId(id);
            medicalfacility.setModified_By(1L);
            medicalfacility.setModified_On(new Date());
            this.medicalFacilityRepo.save(medicalfacility);
            ResponseEntity rest =new ResponseEntity<>("Success", HttpStatus.OK);
            return rest;
        }
        else
        {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("deletemedicalfacility/{id}")
    public String DeleteMedicalFacility(@PathVariable("id") Long id)
    {
        try {
            MedicalFacility medicalfacility = this.medicalFacilityRepo.findByIdData(id);
            medicalfacility.setDeleted_By(1L);
            medicalfacility.setDeleted_On(new Date());
            medicalfacility.setIs_Delete(true);
            this.medicalFacilityRepo.save(medicalfacility);
            return "ok";
        }
        catch (Exception exception)
        {
            return "Data Not Found";
        }
    }
}
