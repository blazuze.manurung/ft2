$(document).ready(function() {
	GetHubunganPasienById();
})

var createdOn;
var createdBy;

function GetHubunganPasienById() {
	var idHubunganPasien = $("#editHubunganPasienId").val();
	$.ajax({
		url: "/api/getbyidhubunganpasien/" + idHubunganPasien,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
		    createdOn = data.created_On;
			$("#namaHubunganPasienInput").val(data.name);
		}
	})
}

$("#editHubunganPasienBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editHubunganPasienBtnSave").click(function() {
	var idHubunganPasien = $("#editHubunganPasienId").val();
    var namaHubunganPasien = $("#namaHubunganPasienInput").val().trim();

    if(namaHubunganPasien == ""){
        $("#errNamaHubunganPasien").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNamaHubunganPasien").text("");
    }

    HubunganPasien(function(hubunganPasien) {
        var isNameExist = hubunganPasien.some(function(item) {
            return item.name.toLowerCase() === namaHubunganPasien.toLowerCase();
        });

        if (isNameExist) {
            $("#errNamaHubunganPasien").text("Nama sudah ada!");
            return;
        } else {
            $("#errNamaHubunganPasien").text("");
        }

	    var obj = {};
	    obj.created_By = createdBy;
	    obj.created_On = createdOn;
	    obj.id = idHubunganPasien;
        obj.name = namaHubunganPasien;

	    var myJson = JSON.stringify(obj);

	    $.ajax({
	    	url: "/api/edithubunganpasien/" + idHubunganPasien,
	    	type: "PUT",
	    	contentType: "application/json",
	    	data: myJson,
	    	success: function(data) {
	    			$(".modal").modal("hide")
	    			location.reload();
	    	},
	    	error: function() {
	    		alert("Terjadi kesalahan")
	    	}
	    });
    });
})

function HubunganPasien(callback) {
    $.ajax({
        url: "/api/getallhubunganpasien",
        type: "GET",
        contentType: "application/json",
        success: function(hubunganPasien) {
            if (callback && typeof callback === "function") {
                callback(hubunganPasien);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}