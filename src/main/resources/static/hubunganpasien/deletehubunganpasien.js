$(document).ready(function() {
	GetHubunganPasienById();

})

var createdOn;
var createdBy;

function GetHubunganPasienById() {
	var idHubunganPasien = $("#deleteHubunganPasienId").val();
	$.ajax({
		url: "/api/getbyidhubunganpasien/" + idHubunganPasien,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
            createdOn = data.created_On;
			$("#namaHubunganPasienDel").text(data.name);
		}
	})
}

$("#deleteHubunganPasienBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteHubunganPasienBtnDelete").click(function() {
	var idHubunganPasien = $("#deleteHubunganPasienId").val();
	$.ajax({
		url : "/api/deletehubunganpasien/" + idHubunganPasien,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})