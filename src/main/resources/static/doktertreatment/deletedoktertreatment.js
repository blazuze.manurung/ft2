$(document).ready(function() {
	GetAllTreatment();

})

var createdOn;
var createdBy;

function GetAllTreatment() {
	var id_treatment = $("#treatmentDelId").val();
	$.ajax({
		url: "/api/getbyiddoktertindakan/" + id_treatment,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		     console.log(data);
			 $("#namaTreatmentDel").text(data.name);
		}
	})
}


$("#delTreatmentBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#delTreatmentBtnDelete").click(function() {
	var id_treatment = $("#treatmentDelId").val();
	$.ajax({
		url : "/api/deletedoktertreatment/"+ id_treatment,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
		console.log(data);
				$(".modal").modal("hide")
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})