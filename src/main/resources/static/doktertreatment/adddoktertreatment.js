$("#addTreatmentBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addTreatmentBtnCreate").click(function(){
	var treatment = $("#treatmentInput").val();
    var dokterId = $("#dokterId").val();

	if(treatment == ""){
		$("#errName").text("Tindakan tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	var obj = {};
	obj.name = treatment;
	obj.doctor_id = dokterId;
	var myJson = JSON.stringify(obj);

	$.ajax({
		url : "/api/adddoktertreatment",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})