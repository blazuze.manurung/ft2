function GetAllNominal() {
    $("#nominalTable").html(
        `<thead>
            <tr>
                <th width='60%'>Nominal</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="nominalTBody"></tbody>
        <tfoot>
            <tr>
                <td colspan="2">
                    <label for="sortOrder">Sort Order:</label>
                    <select id="sortOrder" onchange="sortNominals()">
                        <option value="asc" selected>Ascending</option>
                        <option value="desc">Descending</option>
                    </select>
                </td>
            </tr>
        </tfoot>`
    );

    sortNominals();
}

function sortNominals() {
    const sortOrder = $("#sortOrder").val();
    const data = retrieveNominals();

    data.sort((a, b) => sortOrder === 'asc' ? a.nominal - b.nominal : b.nominal - a.nominal);

    displayNominals(data);
}

function displayNominals(data) {
    $("#nominalTBody").empty();

    if (!data) {
        data = retrieveNominals();
    }

    for (let i = 0; i < data.length; i++) {
        var nominalString = data[i].nominal.toString(),
            sisa = nominalString.length % 3,
            rupiah = nominalString.substr(0, sisa),
            ribuan = nominalString.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        $("#nominalTBody").append(
            `<tr>
                <td>${"Rp. " + rupiah}</td>
                <td>
                    <button value="${data[i].id}" onclick="editNominalDompetElektronik(this.value)" class="btn btn-warning">
                        <i class="bi-pencil-square"></i>
                    </button>
                    <button value="${data[i].id}" onclick="deleteNominalDompetElektronik(this.value)" class="btn btn-danger">
                        <i class="bi-trash"></i>
                    </button>
                </td>
            </tr>`
        );
    }
}

function retrieveNominals() {
    let result;

    $.ajax({
        url: "/api/getallnominaldompetelektronik",
        type: "GET",
        contentType: "application/json",
        async: false,
        success: function (data) {
            result = data;
        }
    });

    return result;
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/nominaldompetelektronik/addnominaldompetelektronik",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Nominal");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editNominalDompetElektronik(id){
	$.ajax({
		url: "/nominaldompetelektronik/editnominaldompetelektronik/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Nominal");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteNominalDompetElektronik(id){
	$.ajax({
		url: "/nominaldompetelektronik/deletenominaldompetelektronik/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Nominal");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function SearchNominal(request) {
//	console.log(request)
	if (request.length > 0)
	{
		$.ajax({
			url: '/api/searchnominaldompetelektronik/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
				let table = "<table class='table table-stripped mt-3'>";
			    table += "<tr> <th width='60%'>Nominal</th> <th>Action</th>"
				if (result.length > 0)
				{
					for (let i = 0; i < result.length; i++) {
					    var	nominalString = result[i].nominal.toString(),
                        sisa 	= nominalString.length % 3,
                        rupiah 	= nominalString.substr(0, sisa),
                        ribuan 	= nominalString.substr(sisa).match(/\d{3}/g);

                        if (ribuan) {
                        separator = sisa ? '.' : '';
                        rupiah += separator + ribuan.join('.');
                        }
						table += "<tr>";
                        table += "<td>" + "Rp. " + rupiah + "</td>";
                        table += "<td><button class='btn btn-warning' value='" + result[i].id + "' onclick=editNominalDompetElektronik(this.value)><i class='bi-pencil-square'></i></button> <button class='btn btn-danger' value='" + result[i].id + "' onclick=deleteNominalDompetElektronik(this.value)><i class='bi-trash'></i></button></td>";
						table += "</tr>";
					}
				} else {
					table += "<tr>";
					table += "<td colspan='2' class='text-center'>No data</td>";
					table += "</tr>";
				}
				table += "</table>";
				$('#nominalTable').html(table);
			}
		});
	} else {
		GetAllNominal();
	}
}
$(document).ready(function(){
GetAllNominal();
})