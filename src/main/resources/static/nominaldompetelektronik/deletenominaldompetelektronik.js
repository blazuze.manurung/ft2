$(document).ready(function() {
	GetNominalById();

})

var createdOn;
var createdBy;

function GetNominalById() {
	var idNominal = $("#deleteNominalId").val();
	$.ajax({
		url: "/api/getbyidnominaldompetelektronik/" + idNominal,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
            createdOn = data.created_On;
			$("#nominalDel").text(data.nominal);
		}
	})
}

$("#deleteNominalBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteNominalBtnDelete").click(function() {
	var idNominal = $("#deleteNominalId").val();
	$.ajax({
		url : "/api/deletenominaldompetelektronik/" + idNominal,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})