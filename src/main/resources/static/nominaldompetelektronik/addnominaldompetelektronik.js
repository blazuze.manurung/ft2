$("#addNominalBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addNominalBtnCreate").click(function(){
	var nominal = $("#nominalInput").val().trim();

    if(nominal == ""){
        $("#errNominal").text("Nominal tidak boleh kosong!");
        return;
    }else{
        $("#errNominal").text("");
    }

    NominalDompetElektronik(function(nominalDompetElektronik) {
        var isNominalExist = nominalDompetElektronik.some(function(item) {
            return item.nominal.toString() === nominal.toString();
        });

        if (isNominalExist) {
            $("#errNominal").text("Nominal sudah ada!");
            return;
        } else {
            $("#errNominal").text("");
        }

	var obj = {};
	obj.nominal = nominal;

	var myJson = JSON.stringify(obj);

	    $.ajax({
	        url : "/api/addnominaldompetelektronik",
	        type : "POST",
	        contentType : "application/json",
	        data : myJson,
	        success: function(data){
	        		$(".modal").modal("hide")
	        		location.reload();
	        },
	        error: function(){
	        	alert("Terjadi kesalahan")
	        }
	    });
    });
})

function NominalDompetElektronik(callback) {
    $.ajax({
        url: "/api/getallnominaldompetelektronik",
        type: "GET",
        contentType: "application/json",
        success: function(nominalDompetElektronik) {
            if (callback && typeof callback === "function") {
                callback(nominalDompetElektronik);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}