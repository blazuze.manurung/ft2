$(document).ready(function() {
	GetNominalById();
})

var createdOn;
var createdBy;

function GetNominalById() {
	var idNominal = $("#editNominalId").val();
	$.ajax({
		url: "/api/getbyidnominaldompetelektronik/" + idNominal,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
		    createdOn = data.created_On;
			$("#nominalInput").val(data.nominal);
		}
	})
}

$("#addNominalBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#addNominalBtnCreate").click(function() {
	var idNominal = $("#editNominalId").val();
    var nominal = $("#nominalInput").val().trim();

    if(nominal == ""){
        $("#errNominal").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errNominal").text("");
    }

    NominalDompetElektronik(function(nominalDompetElektronik) {
        var isNameExist = nominalDompetElektronik.some(function(item) {
            return item.nominal.toString() === nominal.toString();
        });

        if (isNameExist) {
            $("#errNominal").text("Nominal sudah ada!");
            return;
        } else {
            $("#errNominal").text("");
        }

	    var obj = {};
	    obj.created_By = createdBy;
	    obj.created_On = createdOn;
	    obj.id = idNominal;
        obj.nominal = nominal;

	    var myJson = JSON.stringify(obj);

	    $.ajax({
	    	url: "/api/editnominaldompetelektronik/" + idNominal,
	    	type: "PUT",
	    	contentType: "application/json",
	    	data: myJson,
	    	success: function(data) {
	    			$(".modal").modal("hide")
	    			location.reload();
	    	},
	    	error: function() {
	    		alert("Terjadi kesalahan")
	    	}
	    });
    });
})

function NominalDompetElektronik(callback) {
    $.ajax({
        url: "/api/getallnominaldompetelektronik",
        type: "GET",
        contentType: "application/json",
        success: function(nominalDompetElektronik) {
            if (callback && typeof callback === "function") {
                callback(nominalDompetElektronik);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}