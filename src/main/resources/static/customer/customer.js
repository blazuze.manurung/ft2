function GetAllCustomer(){
	$("#customerTable").html(
		`<thead>
			<tr>
				<th>Name</th>>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="customerTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getallcustomer",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#customerTBody").append(
					`
					<tr>
						<td>${data[i].biodata.fullname}</td>
						<td>    
							<button value="${data[i].biodata.id}" onClick="customerMemberDetail(this.value)" class="btn btn-primary">
						    View Details
							</button>
						</td>
					</tr>
					`
				)
			}
		}
	});
}

function customerMemberDetail(id){
    $.ajax({
        url: "/customermember",
        type: "GET",
        contentType: "html",
        success: function(data){
            var idParent = id;
            localStorage.setItem('idParentDetails', idParent);
            window.location.href = "/customermember";
        }
    });
}
$(document).ready(function(){
GetAllCustomer();
})