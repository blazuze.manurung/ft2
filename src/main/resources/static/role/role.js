function getTabelKolomRole(){
    $("#roleTabel").html(
        `<thead>
            <tr>
                <th>
                    <button value="name" onClick="getPageableTable(this.value)" class="btn container-fluid btn-transparent fw-bold text-start">
                    Nama</button >
                </th>
                <th>
                    <button value="code" onClick="getPageableTable(this.value)" class="btn container-fluid btn-transparent fw-bold text-start">
                    Kode</button >
                </th>
                <th>#</th>
            </tr>
        </thead>
        <tbody id="roleTBody"></tbody>
        `
    );
}

function getPageableTable(keterangan){
    getPageable(pagePublic, 5, keterangan);
}

var pagePublic;
var keteranganPublic;
var kataPublic;
var isAsc = true;


function getPageable(page, ukuranHal, keterangan){
    pagePublic = page;
    keteranganPublic = keterangan;

    if(keterangan === "mapped"){
        $.ajax({
            url : "/api/role/mapped?halaman="+page+"&ukuran="+ukuranHal,
            type : "GET",
            contentType : "application/json",
            success: function(data){
                setPageable(data, ukuranHal, keterangan);
            }
        });
    }else if(keterangan === "search"){
        var kata = kataPublic;
        $.ajax({
            url : "/api/role/search?halaman="+page+"&ukuran="+ukuranHal+"&kata="+kata,
            type : "GET",
            contentType : "application/json",
            success: function(data){
                setPageable(data, ukuranHal, keterangan);
            }
        });
    }else{
        $.ajax({
            url : "/api/role/sort?halaman="+page+"&ukuran="+ukuranHal+"&kata="+keterangan+"&isAsc="+isAsc,
            type : "GET",
            contentType : "application/json",
            success: function(data){
                console.log("is asc = "+ isAsc);
                isAsc = isAsc == true ? false : true;
                setPageable(data, ukuranHal, keterangan);
            }
        });
    }
}
function setPageable(data, ukuranHal, keterangan){
        $("#roleTBody").html(``);
        var halSekarang = data.halamanSekarang;
        var halTotal = data.totalHalaman;
        for(i = 0; i < data.role.length; i++){
            $("#roleTBody").append(
            `
            <tr>
                <td>${(data.role[i]).name}</td>
                <td>${(data.role[i]).code}</td>
                <td>
                    <button value="${data.role[i].id}" onClick="editRole(this.value)" class="btn btn-primary">
                        <i class="bi-pencil-square">Ubah</i>
                    </button>
                    <button value="${data.role[i].id}" onClick="deleteRole(this.value)" class="btn btn-danger">
                        <i class="bi-trash">Hapus</i>
                    </button>
                </td>
            </tr>
            `
            )
        }

        $("#halamanNav").html(``);
        if(halSekarang !=0){
            $("#halamanNav").append(
            `
                <li class="page-item"><a class="page-link" href="#${(halSekarang)}" onClick="getPageable(${(halSekarang - 1)}, ${ukuranHal}, '${keterangan}')"><<</a></li>
            `
            );
        }
        for(i = 0; i<data.totalHalaman; i++){
            if(i == data.halamanSekarang){
                $("#halamanNav").append(
                `
                    <li class="page-item disabled"><a class="page-link" onClick="getPageable(${(i)}, ${ukuranHal}, '${keterangan}')">${(i+1)}</a></li>
                `
                );
            }else{
                $("#halamanNav").append(
                `
                    <li class="page-item"><a class="page-link" href="#${(i + 1)}" onClick="getPageable(${(i)}, ${ukuranHal}, '${keterangan}')">${(i+1)}</a></li>
                `
                );
            }
        }

        if(halSekarang != (halTotal - 1) && halTotal> 0){
            $("#halamanNav").append(
            `
                <li class="page-item"><a class="page-link" href="#${(halSekarang+2)}" onClick="getPageable(${(halSekarang + 1)}, ${ukuranHal}, '${keterangan}')">>></a></li>
            `
            );
        }
    //};
}

var opsiUkuranData = document.getElementById("opsiUkuranHalaman"); //untuk mengubah tampilan sesuai dengan pilihan 5 per halaman/ 10 per halaman
opsiUkuranData.onchange = function(){
    var valOpsiUkuranData = opsiUkuranData.value;
    getPageable(0, valOpsiUkuranData);
}

$("#searchBtn").click(function(){
    kataPublic = $("#search").val();
    getPageable(0,5,"search");
//    var kata = $("#search").val(); // ambi value dari kolom search
//	$.ajax({
//		url: "/api/searchrole=" + kata,
//		type: "GET",
//		contentType: "html",
//		success: function(data){
//		    $("#roleTBody").html(``); //untuk menghilangkan isi content dulu saat cari di search
//			for(i = 0; i<data.length; i++){
//            	$("#roleTBody").append(
//                    `
//                    <tr>
//                        <td>${data[i].name}</td>
//                        <td>${data[i].code}</td>
//                        <td>
//                            <button value="${data[i].id}" onClick="editRole(this.value)" class="btn btn-primary">
//                            <i>Ubah</i>
//                            </button>
//                            <button value="${data[i].id}" onClick="deleteRole(this.value)" class="btn btn-danger">
//                            <i>Hapus</i>
//                            </button>
//                        </td>
//                    </tr>
//
//                    `
//
//                )
//            }
//		}
//	});
})

$("#addButton").click(function(){
	$.ajax({
		url: "/role/addrole",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Tambah Hak Akses");
			$(".modal-body").html(data);
			$(".modal").modal("show");

		}
	});
})


function editRole(id){
	$.ajax({
		url: "/role/editrole/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Ubah Hak Akses");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteRole(id){
	$.ajax({
		url: "/role/deleterole/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Hapus Hak Akses");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

$(document).ready(function(){
	getTabelKolomRole();
	getPageable(0, 5, "mapped");
})