$(document).ready(function() {
	GetCustomerMemberById();
})

var createdOn;
var createdBy;

function GetCustomerMemberById() {
	var idCustomerMember = $("#deleteCustomerMemberId").val();
	$.ajax({
		url: "/api/getbyidcustomermember/" + idCustomerMember,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		var idCustomer = data.customer_id;
		localStorage.setItem('idCustomerForDelete', idCustomer);
		    $.ajax({
            	url: "/api/getbyidcustomer/" + idCustomer,
            	type: "GET",
            	contentType: "application/json",
            	success: function(data) {
            	var idBiodata = data.biodata_id;
            	localStorage.setItem('idBiodataForDelete', idBiodata);
            	    $.ajax({
                    	url: "/api/getbyidbiodata/" + idBiodata,
                    	type: "GET",
                    	contentType: "application/json",
                    	success: function(data) {
                    	    createdBy = data.created_By;
                            createdOn = data.created_On;
                    		$("#namaCustomerMemberDel").text(data.fullname);
                    	}
                    });
            	}
            });
		}
	});
}

$("#deleteCustomerMembernBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteCustomerMemberBtnDelete").click(function() {
	var idCustomerMember = $("#deleteCustomerMemberId").val();
	$.ajax({
		url : "/api/deletecustomermember/" + idCustomerMember,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
		var idCustomer = localStorage.getItem('idCustomerForDelete');
            $.ajax({
            	url : "/api/deletecustomer/" + idCustomer,
            	type : "DELETE",
            	contentType : "application/json",
            	success: function(data){
            	var idBiodata = localStorage.getItem('idBiodataForDelete');
                    $.ajax({
                    	url : "/api/deletebiodata/" + idBiodata,
                    	type : "DELETE",
                    	contentType : "application/json",
                    	success: function(data){
                            $(".modal").modal("hide")
                            location.reload();
                    	},
                    	error: function(){
                    		alert("Terjadi kesalahan")
                    	}
                    });
            	},
            	error: function(){
            		alert("Terjadi kesalahan")
            	}
            });
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})