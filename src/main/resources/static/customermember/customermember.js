function GetAllCustomerMember(){
	$("#customerMemberTable").html(
		`<thead>
			<tr>
				<th>Name</th>
				<th>Hubungan Pasien</th>
				<th>Umur</th>
				<th>Chat Online</th>
				<th>Janji Dokter</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="customerMemberTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getallcustomermember",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
			    var dobString = data[i].customer.dob;
			    var dobDate = new Date(dobString);
			    var lahir = dobDate.getFullYear();
			    var now = new Date().getFullYear();
			    var umur = now - lahir;
				    $("#customerMemberTBody").append(
					`
					<tr>
						<td>${data[i].customer.biodata.fullname}</td>
						<td>${data[i].hubunganPasien.name}</td>
						<td>${umur + " Tahun"}</td>
						<td></td>
						<td></td>
						<td>
							<button value="${data[i].id}" onClick="editCustomerMember(this.value)" class="btn btn-warning">
								<i class="bi-pencil-square"></i>
							</button>
							<button value="${data[i].id}" onClick="deleteCustomerMember(this.value)" class="btn btn-danger">
								<i class="bi-trash"></i>
							</button>
							<input type="checkbox" class="delete-checkbox checkbox-custom" value="${data[i].id}">
						</td>
					</tr>
					`
				)
			}
		}
	});
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function CustomerMemberList(currentPage, length) {
	$.ajax({
		url : '/api/customermembermapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = "<table class='table table-stripped mt-3'>";
			table += "<tr> <th>Name</th> <th>Hubungan Pasien</th> <th>Umur</th> <th>Chat Online</th> <th>Janji Dokter</th> <th>Action</th>"
			for (let i = 0; i < data.customerMember.length; i++) {
			    var dobString = data.customerMember[i].customer.dob;
                var dobDate = new Date(dobString);
                var lahir = dobDate.getFullYear();
                var now = new Date().getFullYear();
                var umur = now - lahir;
				table += "<tr>";
				table += "<td>" + data.customerMember[i].customer.biodata.fullname + "</td>";
				table += "<td>" + data.customerMember[i].hubunganPasien.name + "</td>";
				table += "<td>" + umur + " Tahun" + "</td>";
				table += "<td>" + + "</td>";
				table += "<td>" + + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.customerMember[i].id + "' onclick=editCustomerMember(this.value)><i class='bi-pencil-square'></i></button>";
				table += "<button class='btn btn-danger btn-sm' value='" + data.customerMember[i].id + "' onclick=deleteCustomerMember(this.value)><i class='bi-trash'></i></button>";
				table += "<input type='checkbox' class='delete-checkbox checkbox-custom' value='" + data.customerMember[i].id +"'></td>";
				table += "</tr>";
			}
			table += "</table>";
			table += '<nav aria-label="Page navigation">';
			table += '<ul class="pagination mt-2">'
			table += '<li class="page-item"><a class="page-link" onclick="CustomerMemberList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
			let index = 1;
			for (let i = 0; i < data.totalPages; i++) {
				table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="CustomerMemberList(' + i + ',' + length + ')">' + index + '</a></li>'
				index++;
			}
			table += '<li class="page-item"><a class="page-link" onclick="CustomerMemberList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#customerMemberList').html(table);
		}
	});
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/customermember/addcustomermember",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Member Pasien");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})

function editCustomerMember(id){
	$.ajax({
		url: "/customermember/editcustomermember/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Member Pasien");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteCustomerMember(id){
	$.ajax({
		url: "/customermember/deletecustomermember/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Member Pasien");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteSelectedItems() {
    var selectedIds = $(".delete-checkbox:checked").map(function(){
        return $(this).val();
    }).get();

    console.log(selectedIds);
    var customerNames = [];

    selectedIds.forEach(function(id) {
        console.log("this is " + id);
        $.ajax({
        		url : "/api/getbyidcustomermember/" + id,
        		type : "GET",
        		contentType : "application/json",
        		async : false,
        		success: function(data){
//        		$("#customerDeleteBody").append(
//                    `
//                    <td>${data.customer.biodata.fullname}</td>
//                    `
//                    )
//        		}
                customerNames.push(data.customer.biodata.fullname);
                }
            })
        })
    console.log(customerNames);
    if (selectedIds.length > 1) {
        $(".modal").modal("show");
        $(".modal-title").text("Delete Member Pasien");

        let table = "<table class='table table-borderless table-sm'>"
        table += "<th>Nama Member Pasien</th>"
        for(let i = 0; i < customerNames.length; i++)
        {
            table += "<tr>"
            table += "<td>" + customerNames[i] + "</td>"
            table += "</tr>";
        }
        table += "</table>"
        table += "<button id='deleteCustomerMembernBtnCancel' class='btn btn-outline-dark'>Cancel</button>"
        table += "<button id='deleteCustomerMemberBtnDelete' class='btn btn-danger'>Delete</button>"

        $(".modal-body").html(table);
        $("#deleteCustomerMembernBtnCancel").click(function() {
        	$(".modal").modal("hide")
        })
        $("#deleteCustomerMemberBtnDelete").click(function() {
        for(let i = 0; i < selectedIds.length; i++)
            $.ajax({
                url: "/api/deletemultiplecustomermember",
                type: "DELETE",
                contentType: "application/json",
                data: JSON.stringify(selectedIds),
                success: location.reload(),
            });
        })

//        `
//            <table class="table table-borderless table-sm">
//        	    <tr>
//        		    <th>Nama Member Pasien</th>
//        		    <td></td>
//        	    </tr>
//            </table>
//
//        <button id="deleteCustomerMembernBtnCancel" class="btn btn-outline-dark">Cancel</button>
//        <button id="deleteCustomerMemberBtnDelete" class="btn btn-danger">Delete</button>
//
//        <script src="../customermember/deletecustomermember.js"></script>
//        `

    }else {
        $(".modal-title").text("Delete Items");
        $(".modal-body").html("Please select at least two items to delete.");
        $(".modal").modal("show");
    }
}
function SearchCustomerMember(request) {
	//console.log(request)
	if (request.length > 0)
	{
		$.ajax({
			url: '/api/searchcustomermember/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
				//console.log(result)
				let table = "<table class='table table-stripped mt-3'>";
                table += "<tr> <th>Name</th> <th>Hubungan Pasien</th> <th>Umur</th> <th>Chat Online</th> <th>Janji Dokter</th> <th>Action</th>"
                if (result.length > 0)
				{
					for (let i = 0; i < result.length; i++) {
                    	var dobString = result[i].customer.dob;
                        var dobDate = new Date(dobString);
                        var lahir = dobDate.getFullYear();
                        var now = new Date().getFullYear();
                        var umur = now - lahir;
                    	table += "<tr>";
                    	table += "<td>" + result[i].customer.biodata.fullname + "</td>";
                    	table += "<td>" + result[i].hubunganPasien.name + "</td>";
                    	table += "<td>" + umur + " Tahun" + "</td>";
                    	table += "<td>" + + "</td>";
                    	table += "<td>" + + "</td>";
                    	table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editCustomerMember(this.value)><i class='bi-pencil-square'></i></button>";
                    	table += "<button class='btn btn-danger btn-sm' value='" +result[i].id + "' onclick=deleteCustomerMember(this.value)><i class='bi-trash'></i></button>";
                    	table += "<input type='checkbox' class='delete-checkbox checkbox-custom' value='" + result[i].id +"'></td>";
                    	table += "</tr>";
					}
				} else {
					table += "<tr>";
					table += "<td colspan='6' class='text-center'>No data</td>";
					table += "</tr>";
				}
				table += "</table>";
				$('#customerMemberList').html(table);
			}
		});
	} else {
		CustomerMemberList(0,5);
	}
}

$(document).ready(function(){
CustomerMemberList(0,5);
})