$(document).ready(function() {
	GetCustomerMemberById();
	opsiReload();
	getId();
})

function getId(){
var idCustomerMember = $("#editCustomerMemberId").val();
    $.ajax({
        url : "/api/getbyidcustomermember/" + idCustomerMember,
        type : "GET",
        contentType : "application/json",
        success: function(data){
            var idCustomer = data.customer_id;
            localStorage.setItem('idCustomerForEdit', idCustomer);
            $.ajax({
                url : "/api/getbyidcustomer/" + idCustomer,
                type : "GET",
                contentType : "application/json",
                success: function(data){
                  var idBiodata = data.biodata_id;
                  localStorage.setItem('idBiodataForEdit', idBiodata);
                }
            });
        }
    });
}

function opsiReload(){
   var jkSel = document.getElementById("opsiJenisKelamin");
   var golDarSel = document.getElementById("opsiGolonganDarah");
   var rhesusSel = document.getElementById("opsiRhesus");
   var relasiSel = document.getElementById("opsiRelasi");

   jkSel.options[jkSel.options.length] = new Option("Pria", "P");
   jkSel.options[jkSel.options.length] = new Option("Wanita", "W");

   rhesusSel.options[rhesusSel.options.length] = new Option("Rh +", "Plus");
   rhesusSel.options[rhesusSel.options.length] = new Option("Rh -", "Minus");

   $.ajax({
       url : "/api/getallgolongandarah",
       type : "GET",
       contentType : "application/json",
       success: function(data){
           for(i = 0; i<data.length; i++){
               golDarSel.options[golDarSel.options.length] = new Option(data[i].code, data[i].id);
           }
       }
   });

   $.ajax({
       url : "/api/getallhubunganpasien",
       type : "GET",
       contentType : "application/json",
       success: function(data){
           for(i = 0; i<data.length; i++){
               relasiSel.options[relasiSel.options.length] = new Option(data[i].name, data[i].id);
           }
       }
   });
}

var createdOn;
var createdBy;

function GetCustomerMemberById() {
	var idCustomerMember = $("#editCustomerMemberId").val();
	$.ajax({
		url: "/api/getbyidcustomermember/" + idCustomerMember,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
		    createdOn = data.created_On;
			$("#namaMemberPasienInput").val(data.customer.biodata.fullname);
			$("#dobMember").val(data.customer.dob);
			$("#opsiJenisKelamin").val(data.customer.gender);
			$("#opsiGolonganDarah").val(data.customer.golonganDarah.id);
			$("#opsiRhesus").val(data.customer.rhesus_Type);
			$("#tinggiBadanInput").val(data.customer.height);
			$("#beratBadanInput").val(data.customer.weight);
			$("#opsiRelasi").val(data.hubunganPasien.id);
		}
	})
}

$("#editCustomerMemberBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editCustomerMemberBtnSave").click(function() {
	var idCustomerMember = $("#editCustomerMemberId").val();
    var namaMember = $("#namaMemberPasienInput").val();
    var tanggalLahir = $("#dobMember").val();
    var jenisKelamin = $("#opsiJenisKelamin").val();
    var golonganDarah = $("#opsiGolonganDarah").val();
    var rhesus = $("#opsiRhesus").val();
    var tinggiBadan = $("#tinggiBadanInput").val();
    var beratBadan = $("#beratBadanInput").val();
    var relasi = $("#opsiRelasi").val();


    if(namaMember == ""){
    	$("#errNamaMemberPasien").text("Nama tidak boleh kosong!");
    	return;
    } else {
    	$("#errNamaMemberPasien").text("");
    }
    if(tanggalLahir == ""){
    	$("#errDobMember").text("Tanggal Lahir tidak boleh kosong!");
    	return;
    } else {
    	$("#errDobMember").text("");
    }
    if(jenisKelamin == ""){
    	$("#errJenisKelamin").text("Jenis Kelamin tidak boleh kosong!");
    	return;
    } else {
    	$("#errJenisKelamin").text("");
    }
    if(golonganDarah == ""){
    	$("#errGolonganDarah").text("Golongan Darah tidak boleh kosong!");
    	return;
    } else {
    	$("#errGolonganDarah").text("");
    }
    if(rhesus == ""){
    	$("#errRhesus").text("Rhesus tidak boleh kosong!");
    	return;
    } else {
    	$("#errRhesus").text("");
    }
    if(tinggiBadan == ""){
    	$("#errTinggiBadan").text("Tinggi Badan tidak boleh kosong!");
    	return;
    } else {
    	$("#errTinggiBadan").text("");
    }
    if(beratBadan == ""){
    	$("#errBeratBadan").text("Berat Badan tidak boleh kosong!");
    	return;
    } else {
    	$("#errBeratBadan").text("");
    }
    if(relasi == ""){
    	$("#errRelasi").text("Relasi tidak boleh kosong!");
    	return;
    } else {
    	$("#errRelasi").text("");
    }

    var idParent = localStorage.getItem('idParentDetails');
    var idCustomer = localStorage.getItem('idCustomerForEdit');
	var obj = {};
	obj.created_By = createdBy;
	obj.created_On = createdOn;
	obj.id = idCustomerMember;
	obj.customer_relation_id = relasi;
	obj.parent_biodata_id = idParent;
	obj.customer_id = idCustomer;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editcustomermember/" + idCustomerMember,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
		    var idBiodata = localStorage.getItem('idBiodataForEdit');
		    var idCustomer = data.customer_id;
            var objToCustomer = {};
            objToCustomer.id = idCustomer;
            objToCustomer.biodata_id = idBiodata;
            objToCustomer.created_By = createdBy;
            objToCustomer.created_On = createdOn;
            objToCustomer.dob = tanggalLahir;
            objToCustomer.gender = jenisKelamin;
            objToCustomer.blood_group_id = golonganDarah;
            objToCustomer.rhesus_Type = rhesus;
            objToCustomer.height = tinggiBadan;
            objToCustomer.weight = beratBadan;

            var myJsonToCustomer = JSON.stringify(objToCustomer);

        $.ajax({
            url: "/api/editcustomer/" + idCustomer,
            type: "PUT",
            contentType: "application/json",
            data: myJsonToCustomer,
            success: function(data) {
                var idBiodata = data.biodata_id;
                var objToBiodata = {};
                objToBiodata.id = idBiodata;
                objToBiodata.created_By = createdBy;
                objToBiodata.created_On = createdOn;
                objToBiodata.fullname = namaMember;

                var myJsonToBiodata = JSON.stringify(objToBiodata);

            $.ajax({
                url : "/api/editbiodata/" + idBiodata,
                type : "PUT",
                contentType : "application/json",
                data : myJsonToBiodata,
                success: function(data){
                console.log(data);
                		$(".modal").modal("hide")
                		location.reload();
                },
	            error: function(){
                    alert("Terjadi kesalahan")
                }
                });
            },
            error: function(){
            	alert("Terjadi kesalahan")
            }
            });
       	},
       	error: function(){
       	    alert("Terjadi kesalahan")
       	}
    });
})