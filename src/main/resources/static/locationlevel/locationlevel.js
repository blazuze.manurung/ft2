function GetAllLocationLevel(){
	$("#locationLevelTable").html(
		`<thead>
			<tr>
				<th>ID</th>
				<th>Nama</th>
				<th>Nama Singkat</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="locationLevelTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getalllocationlevel",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#locationLevelTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${data[i].name}</td>
						<td>${data[i].abbreviation}</td>
						<td>
							<button value="${data[i].id}" onClick="editLocationLevel(this.value)" class="btn btn-warning">
								<i class="bi-pencil-square"></i>
							</button>
							<button value="${data[i].id}" onClick="deleteLocationLevel(this.value)" class="btn btn-danger">
								<i class="bi-trash"></i>
							</button>
						</td>
					</tr>
					`
				)
			}
		}
	});
}
function editLocationLevel(id){
	$.ajax({
		url: "/locationlevel/editlocationlevel/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Location Level");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}
function deleteLocationLevel(id){
	$.ajax({
		url: "/locationlevel/deletelocationlevel/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Hubungan Pasien");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}
$("#addBtn").click(function(){
	$.ajax({
		url: "/locationlevel/addlocationlevel",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Hubungan Pasien");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})
function LocationLevelList(currentPage, length) {
    $.ajax({
		url : '/api/locationlevelmapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = '<select class="custom-select mt-3" id="size" onchange="LocationLevelList (0,this.value)">'
//			table += '<option value="3" selected>..</option>'
			table += '<option value="5">5</option>'
			table += '<option value="10">10</option>'
			table += '<option value="15">15</option>'
			table += '</select>'
			table += "<table class='table table-bordered mt-3'>";
			table += "<tr> <th>Id</th> <th >Name</th> <th>Nama Singkat</th> <th>Action</th></tr>"
			for (let i = 0; i < data.locationlevel.length; i++) {
				table += "<tr>";
				table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
				table += "<td>" + data.locationlevel[i].name + "</td>";
				table += "<td>" + data.locationlevel[i].abbreviation + "</td>";
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.locationlevel[i].id + "' onclick=editLocationLevel(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" +  data.locationlevel[i].id + "' onclick=deleteLocationLevel(this.value)>Delete</button></td>";
				// table += "<td><input type='checkbox' onclick='SelectedItem()' class='mychecked' id='listdelete' value='"+data.category[i].id+"'></td>"
				table += "</tr>";
			}
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="LocationLevelList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="LocationLevelList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
				}
			table += '<li class="page-item"><a class="page-link" onclick="LocationLevelList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#locationlevelList').html(table);
		}

	});
}
//function SearchLocationLevel(request) {
//	//console.log(request)
//	if (request.length > 0)
//	{
//		$.ajax({
//			url: '/api/searchlocationlevel/' + request,
//			type: 'GET',
//			contentType: 'application/json',
//			success: function (result) {
//				//console.log(result)
//				let table = "<table class='table table-stripped mt-3'>";
//			    table += "<tr> <th>Id</th> <th >Name</th> <th>Nama Singkat</th> <th>Action</th>"
//				if (result.length > 0)
//				{
//					for (let i = 0; i < result.length; i++) {
//						table += "<tr>";
//						table += "<td>" + result[i].id + "</td>";
//                        table += "<td>" + result[i].name + "</td>";
//                        table += "<td>" + result[i].abbreviation + "</td>";
//                        table += "<td><button class='btn btn-warning' value='" + result[i].id + "' onclick=editLocationLevel(this.value)><i class='bi-pencil-square'></i></button> <button class='btn btn-danger' value='" + result[i].id + "' onclick=deleteLocationLevel(this.value)><i class='bi-trash'></i></button></td>";
//						table += "</tr>";
//					}
//				} else {
//					table += "<tr>";
//					table += "<td colspan='2' class='text-center'>No data</td>";
//					table += "</tr>";
//				}
//				table += "</table>";
//				$('#locationLevelTable').html(table);
//			}
//		});
//	} else {
//		GetAllLocationLevel();
//	}
//}

function SearchLocationLevel(request) {
    if (request.length > 0) {
        $.ajax({
            url: '/api/searchlocationlevel/' + request,
            type: 'GET',
            contentType: 'application/json',
            success: function (result) {
                let table = "<table class='table table-bordered mt-3'>";
                table += "<tr> <th width='10%' class='text-center'>ID </th> <th>Nama</th> <th>Nama Singkat</th> <th>Action</th></tr>"
                if (result.length > 0) {
                    for (let i = 0; i < result.length; i++) {
                        table += "<tr>";
                        table += "<td class='text-center'>" + (i + 1) + "</td>";
                        table += "<td>" + result[i].name + "</td>";
                        table += "<td>" + result[i].abbreviation + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editLocationLevel(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + result[i].id + "' onclick=deleteLocationLevel(this.value)>Delete</button></td>";
                        table += "</tr>";
                    }
                } else {
                    table += "<tr>";
                    table += "<td colspan='5' class='text-center'>No data</td>";
                    table += "</tr>";
                    // Display an alert here since there is no data
                    alert("Data Yang Anda Cari Tidak Ada");
                }
                table += "</table>";
                $('#locationlevelList').html(table);
            }
        });
    } else {
        LocationLevelList(0, 5);
    }
}

$(document).ready(function(){
LocationLevelList(0,5);
});