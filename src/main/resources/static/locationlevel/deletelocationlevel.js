$(document).ready(function() {
    GetLocationLevelById();
});

var createdOn;
var createdBy;

function GetLocationLevelById() {
    var idLocationLevel = $("#deleteLocationLevelId").val();
    $.ajax({
        url: "/api/getbyidlocationlevel/" + idLocationLevel,
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            createdBy = data.created_By;
            createdOn = data.created_On;
            $("#nameLocationLevelDel").text(data.name);
        }
    });
}

$("#deleteBtnCancel").click(function() {
    $(".modal").modal("hide");
});

$("#deleteBtnDelete").click(function() {
    var idLocationLevel = $("#deleteLocationLevelId").val();

    console.log("Deleting location level with ID:", idLocationLevel);

    $.ajax({
        url: "/api/deletelocationlevel/" + idLocationLevel,
        type: "DELETE",
        contentType: "application/json",
        success: function(data) {
            console.log("Deletion successful:", data);
            $(".modal").modal("hide");
            location.reload();
        },
        error: function(xhr, status, error) {
            console.error("Error deleting location level:", status, error);
            alert("Data Ini Masih Dipakai");
        }
    });
});
