$("#addCancelBtn").click(function () {
    $(".modal").modal("hide");
});

$("#addCreateBtn").click(function () {
    // Define an array of objects with information about each input field
    var inputValName = $("#name").val();
    var fields = [
        { input: "#name", error: "#errNama", errorMessage: "Nama tidak boleh kosong!" },
        { input: "#abbreviation", error: "#errNamaSingkat", errorMessage: "Nama singkat tidak boleh kosong!" }
        // Add more fields if needed
    ];

    // Iterate through the fields and perform validation
    for (var i = 0; i < fields.length; i++) {
        var value = $(fields[i].input).val().trim();
        if (value === "") {
            $(fields[i].error).text(fields[i].errorMessage);
            return; // Stop further processing if any field is empty
        } else {
            $(fields[i].error).text("");
        }
    }

    // If all fields are non-empty, proceed with the AJAX request
    var obj = {};
    for (var i = 0; i < fields.length; i++) {
        var fieldName = fields[i].input.substring(1); // Remove '#' from the input ID
        obj[fieldName] = $(fields[i].input).val().trim();
    }

    var myJson = JSON.stringify(obj);
    console.log(myJson);
    $.ajax({
        url: "/api/addlocationlevel",
        type: "POST",
        contentType: "application/json",
        data: myJson,
        success: function (data) {
            $(".modal").modal("hide");
            location.reload();
        },
        error: function (xhr) {
            if (xhr.status === 400) {
                // Display the existing data that matches the input
                var responseJson = xhr.responseJSON;
                if (responseJson && responseJson.message) {
                    alert("Data Sama BAng: " + responseJson.message);
                } else {
                console.log(inputValName);
                SearchLocationLevel(inputValName);
                    alert("Data Yang Anda Masukkan Sudah Ada");

                }
            } else {
                alert("An error occurred. Please try again later.");
            }
        }
    });
});
