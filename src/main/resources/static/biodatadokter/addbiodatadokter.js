$("#addnamaDokterBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addnamaDokterBtnCreate").click(function(){
    //Ambil data dokter by id / nama
	var namaDokter = $("#namaDokterInput").val();
	var nomorStr = $("#nomorStrInput").val();

    if(namaDokter == ""){
        $("#errnamaDokter").text("Nama tidak boleh kosong!");
        return;
    }else{
        $("#errnamaDokter").text("");
    }
    if(nomorStr == ""){
            $("#errnomorStr").text("Nama tidak boleh kosong!");
            return;
        }else{
            $("#errnomorStr").text("");
        }

	var obj = {};
	obj.biodata = namaDokter;
	obj.str = nomorStr;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url : "/api/addbiodatadokter",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})