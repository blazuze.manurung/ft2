function registerOnLogin(){
    $.ajax({
        url: "/register",
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal-title").text("Daftar");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

$("#loginUserBtn").click(function(){
    buttonLoading(true);
    var email = $("#emailInput").val();
    var password = $("#passwordInput").val();
    var booleanEmail = false;
    var booleanPassword = false;
    if(email === ""){
        $("#emailInputErr").text("*Masukkan email");
    }else{
        $("#emailInputErr").text("");
        booleanEmail = true;
    }
    if(password === ""){
        $("#passwordInputErr").text("*Masukkan password");
    }else{
        $("#passwordInputErr").text("");
        booleanPassword = true;
    }

    if(booleanEmail == false || booleanPassword == false){
        buttonLoading(false);
        return;
    }else if (booleanEmail == true && booleanPassword == true){
        $.ajax({
            url:"/api/user/login?email=" + email +"&password=" + password,
            type:"POST",
            contentType: "application/json",
            success: function(data){
                if(data === "success"){
                    buttonLoading(false);
                    alert("Berhasil");
                    location.reload();
                }else if(data === "gagal"){
                    $("#passwordInputErr").text("*Password salah");
                    buttonLoading(false);
                    return;
                }else if(data === "terkunci"){
                    alert("Akun anda terkunci silakan hubungi admin");
                    location.reload();
                }else if(data === "no-email"){
                    $("#emailInputErr").text("*Email tidak terdaftar");
                    buttonLoading(false);
                    return;
                }
            }
        });
    }
})

$("#eyeIconButton").click(function(){
    if($("#passwordInput").attr("type") == "password"){
        $("#passwordInput").attr("type", "text");
        $("#eyeIconLogin").removeClass("bi bi-eye-slash");
        $("#eyeIconLogin").addClass("bi bi-eye");
    }else if($("#passwordInput").attr("type") == "text"){
        $("#passwordInput").attr("type", "password");
        $("#eyeIconLogin").removeClass("bi bi-eye");
        $("#eyeIconLogin").addClass("bi bi-eye-slash");
    }
})

function buttonLoading(keterangan){
    if(keterangan === true){
        document.getElementById("loginUserBtn").disabled = true;
        document.getElementById("loginUserBtn").innerHTML = "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Loading...";
    }else{
        document.getElementById("loginUserBtn").disabled = false;
        document.getElementById("loginUserBtn").innerHTML = "Masuk";
    }
}