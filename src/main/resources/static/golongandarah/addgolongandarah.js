$("#addGolonganDarahBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addGolonganDarahBtnCreate").click(function(){
	var codeGolonganDarah = $("#kodeGolonganDarahInput").val().trim();
	var descriptionGolonganDarah = $("#deskripsiGolonganDarahInput").val();

    if(codeGolonganDarah == ""){
        $("#errkodeGolonganDarah").text("Kode tidak boleh kosong!");
        return;
    }else{
        $("#errkodeGolonganDarah").text("");
    }

    GolonganDarah(function(golonganDarah){
        var isCodeExist = golonganDarah.some(function(item){
            return item.code.toLowerCase() === codeGolonganDarah.toLowerCase();
        });

        if(isCodeExist) {
            $("#errkodeGolonganDarah").text("Kode sudah tersedia");
            return;
        }else{
            $("#errkodeGolonganDarah").text("");
        }

	var obj = {};
	obj.code = codeGolonganDarah;
	obj.description = descriptionGolonganDarah

	var myJson = JSON.stringify(obj);

	$.ajax({
		url         : "/api/addgolongandarah",
		type        : "POST",
		contentType : "application/json",
		data        : myJson,
		success     : function(data){
				$(".modal").modal("hide")
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	    });
	});
})
function GolonganDarah(callback){
    $.ajax({
        url         : "/api/getallgolongandarah",
        type        : "GET",
        contentType : "application/json",
        success     : function(golonganDarah){
                if (callback && typeof callback === "function"){
                    callback(golonganDarah);
                }
        },
        error : function(error){
            if (callback && typeof callback === "function"){
                callback(error);
            }
        }
    });
}