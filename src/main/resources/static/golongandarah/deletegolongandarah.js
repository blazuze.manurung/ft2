$(document).ready(function() {
	getGolonganDarahById();

})

var createdOn;
var createdBy;

function getGolonganDarahById() {
	var id = $("#deleteGolonganDarahId").val();
	$.ajax({
		url: "/api/getbyidgolongandarah/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
            createdOn = data.created_On;
			$("#kodeGolonganDarahDel").text(data.code);
		}
	})
}

$("#deleteGolonganDarahBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#deleteGolonganDarahBtnDelete").click(function() {
	var id = $("#deleteGolonganDarahId").val();
	$.ajax({
		url : "/api/deletegolongandarah/" + id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
            $(".modal").modal("hide")
            location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})