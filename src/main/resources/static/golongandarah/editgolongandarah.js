$(document).ready(function() {
	getGolonganDarahById();
})

var createdOn;
var createdBy;

function getGolonganDarahById() {
	var id = $("#editGolonganDarahId").val();
	$.ajax({
		url: "/api/getbyidgolongandarah/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
		    createdBy = data.created_By;
		    createdOn = data.created_On;
			$("#kodeGolonganDarahInput").val(data.code);
			$("#deskripsiGolonganDarahInput").val(data.description);
		}
	})
}

$("#addGolonganDarahBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#addGolonganDarahBtnCreate").click(function() {
	var id = $("#editGolonganDarahId").val();
    var kodeGolonganDarah = $("#kodeGolonganDarahInput").val();
    var deskripsiGolonganDarah = $("#deskripsiGolonganDarahInput").val().trim();


    if(kodeGolonganDarah == ""){
        $("#errkodeGolonganDarah").text("Kode tidak boleh kosong");
        return;
    }else{
        $("#errkodeGolonganDarah").text("");
    }

    GolonganDarah(function(golonganDarah){
        var isCodeExist = golonganDarah.some(function(item){
            return item.code.toLowerCase() === kodeGolonganDarah.toLowerCase();
        });

        if (isCodeExist){
            $("#errkodeGolonganDarah").text("Kode sudah tersedia");
            return;
        } else{
            $("#errkodeGolonganDarah").text("");
        }


	var obj = {};
	obj.created_By = createdBy;
	obj.created_On = createdOn;
	obj.id = id;
    obj.code = kodeGolonganDarah;
    obj.description = deskripsiGolonganDarah;

	var myJson = JSON.stringify(obj);
//	console.log(myJson);

	$.ajax({
		url: "/api/editgolongandarah/" + id,
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
				$(".modal").modal("hide")
				location.reload();
		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
  });
})
function GolonganDarah(callback) {
    $.ajax({
        url: "/api/getallgolongandarah",
        type: "GET",
        contentType: "application/json",
        success: function(golonganDarah) {
            if (callback && typeof callback === "function") {
                callback(golonganDarah);
            }
        },
        error: function(error) {
            if (callback && typeof callback === "function") {
                callback(error);
            }
        }
    });
}