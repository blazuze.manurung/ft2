$("#addCancelBtn").click(function(){
	$(".modal").modal("hide")
})

$("#addCreateBtn").click(function(){
	var nama = $("#namaInput").val();

	if(nama == ""){
		$("#errNama").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errNama").text("");
	}
	var obj = {};
	obj.name = nama;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url : "/api/addspesialisasidokter",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){
				$(".modal").modal("hide")
				location.reload();
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})