$(document).ready(function() {
	getlocationId();
})

function getlocationId() {
	var id = $("#delId").val();
	console.log("Mengambil data dengan ID:", id);
	$.ajax({
		url: "/api/location/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#namaLocationDel").text(data.name);
		}
	})
}

$("#cancelBtn").click(function() {
	$(".modal").modal("hide")
})

$("#deleteBtn").click(function() {
	var id = $("#delId").val();
	console.log("Menghapus data dengan ID:", id);
	$.ajax({
		url : "/api/location/" + id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data, textStatus, xhr){
		    $(".modal").modal("hide")
			LocationList(0, 5);
		},
		error: function(xhr, textStatus, errorThrown){
			if (xhr.status === 400) {
                 var errorMessage = xhr.responseText;
                 $("#errName").text(errorMessage);
            } else {
                 alert("Terjadi kesalahan: " + errorThrown);
            }
		}
	});
})
