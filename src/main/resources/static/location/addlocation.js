$("#cancelButton").click(function(){
    $(".modal").modal("hide");
});

function getAllOption() {
    $.ajax({
        url: "/api/getalllocationlevel",
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            for (let i = 0; i < data.length; i++) {
                $("#levelSelect").append(
                    `<option value="${data[i].id}">${data[i].name}</option>`
                );
                console.log("ID dari location level:", data[i].id);
            }
        }
    });

    $.ajax({
        url: "/api/getlocation",
        type: "GET",
        contentType: "application/json",
        success: function(data) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].parentLocation) {
                    $("#areaSelect").append(
                        `<option value="${data[i].id}">${data[i].locationLevel.abbreviation} ${data[i].name},
                        ${data[i].parentLocation.locationLevel.abbreviation} ${data[i].parentLocation.name}</option>`
                    );
                } else {
                    $("#areaSelect").append(
                        `<option value="${data[i].id}">${data[i].locationLevel.abbreviation} ${data[i].name}</option>`
                    );
                }
            }
        },
        error: function(error) {
            console.error("Gagal mengambil data lokasi:", error);
        }
    });

}

$("#addButton").click(function() {
    var namaLocation = $("#nameInput").val().trim();
    var selectArea = $("#areaSelect").val();
    var selectLevel = $("#levelSelect").val();

    if (namaLocation === "") {
            $("#errName").text("Nama Level Lokasi tidak boleh kosong");
            return;
        } else {
            $("#errName").text("");
        }

    if (selectLevel === "--Pilih--") {
        $("#errlevelSelect").text("Pilih level lokasi terlebih dahulu!");
        return;
    } else {
        $("#errlevelSelect").text("");
    }

    if (selectLevel === "1") {
        if (selectArea !== "--Pilih--") {
            $("#errareaSelect").text("Wilayah harus kosong atau dipilih saat level lokasi adalah Provinsi.");
            return;
        } else {
             $("#errareaSelect").text("");
        }
    } else {
        if (selectArea === "--Pilih--") {
            $("#errareaSelect").text("Pilih wilayah terlebih dahulu!");
            return;
        } else {
            $("#errareaSelect").text("");
        }
    }

    var obj = {
        name: namaLocation,
        locationLevel: {
            id: selectLevel
        },
        parentLocation: selectArea === "--Pilih--" ? null : { id: selectArea }
    };

    var myJson = JSON.stringify(obj);
    console.log("Data yang di input : ", myJson)
    $.ajax({
        url: "/api/addlocation",
        type: "POST",
        contentType: "application/json",
        data: myJson,
        success: function(data, textStatus, xhr) {
            $(".modal").modal("hide");
            location.reload();
            LocationList(0, 5);
        },
        error: function(xhr, textStatus, errorThrown) {
             if (xhr.status === 400) {
                 var errorMessage = xhr.responseText;
                 $("#errName").text(errorMessage);
             } else {
                 alert("Terjadi kesalahan: " + errorThrown);
             }
        }
    });
});

$(document).ready(function() {
    getAllOption();
});

