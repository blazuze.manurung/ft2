function GetAllLocation(){
	$("#locationTable").html(
		`<thead>
			<tr>
				<th>ID</th>
				<th>Nama</th>
				<th>Level Lokasi</th>
				<th>Wilayah</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="locationLevelTBody"></tbody>
		`
	);

	$.ajax({
		url : "/api/getlocation",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#locationTBody").append(
					`
					<tr>
						<td>${data[i].id}</td>
						<td>${data[i].name}</td>
						<td>${(data[i].m_location_level).name}</td>
						<td>${data[i].name}</td>

						<td>
							<button value="${data[i].id}" onClick="editLocation(this.value)" class="btn btn-warning">
								<i class="bi-pencil-square"></i>
							</button>
							<button value="${data[i].id}" onClick="deleteLocation(this.value)" class="btn btn-danger">
								<i class="bi-trash"></i>
							</button>
						</td>
					</tr>
					`
				)
			}
		}
	});
}
function editLocation(id){
	$.ajax({
		url: "/location/editlocation/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Location");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}
function deleteLocation(id){
	$.ajax({
		url: "/location/deletelocation/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Hubungan Pasien");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}
$("#addBtn").click(function(){
	$.ajax({
		url: "/location/addlocation",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Hubungan Pasien");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})
function LocationList(currentPage, length) {
    $.ajax({
		url : '/api/locationmapped?page=' + currentPage + '&size=' + length,
		type : 'GET',
		contentType : 'application/json',
		success : function(data) {

			let table = '<select class="custom-select mt-3" id="size" onchange="LocationList (0,this.value)">'
//			table += '<option value="3" selected>..</option>'
			table += '<option value="5">5</option>'
			table += '<option value="10">10</option>'
			table += '<option value="15">15</option>'
			table += '</select>'
			table += "<table class='table table-bordered mt-3'>";
			table += "<tr> <th>Id</th> <th >Name</th> <th>Level Lokasi</th> <th>Wilayah</th> <th>Action</th> </tr>"
			for (let i = 0; i < data.location.length; i++) {
				table += "<tr>";
				table += "<td class='text-center'>" + ((i + 1) + (data.currentPage * length)) + "</td>";
				table += "<td>" + data.location[i].name + "</td>";
				table += "<td>" + data.location[i].locationLevel.name + "</td>";
                table += '<td>';
                table += data.location[i].parentLocation ? `${data.location[i].parentLocation.locationLevel.abbreviation} ${data.location[i].parentLocation.name}` : 'N/A';
                table += '</td>';
				table += "<td><button class='btn btn-primary btn-sm' value='" + data.location[i].id + "' onclick=editLocation(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" +  data.location[i].id + "' onclick=deleteLocation(this.value)>Delete</button></td>";
				table += "</tr>";
			    }
				table += "</table>";
				table += "<br>"
				table += '<nav aria-label="Page navigation">';
				table += '<ul class="pagination">'
				table += '<li class="page-item"><a class="page-link" onclick="LocationList(' + (data.currentPage - 1) + ',' + length + ')">Previous</a></li>'
				let index = 1;
				for (let i = 0; i < data.totalPages; i++) {
					table += '<li class="page-item"><a id="pageslink" class="page-link" onclick="LocationList(' + i + ',' + length + ')">' + index + '</a></li>'
					index++;
				}
			table += '<li class="page-item"><a class="page-link" onclick="LocationList(' + (data.currentPage + 1) + ',' + length + ')">Next</a></li>'
			table += '</ul>'
			table += '</nav>';
			$('#locationList').html(table);
		}

	});
}
//function SearchLocationLevel(request) {
//	//console.log(request)
//	if (request.length > 0)
//	{
//		$.ajax({
//			url: '/api/searchlocationlevel/' + request,
//			type: 'GET',
//			contentType: 'application/json',
//			success: function (result) {
//				//console.log(result)
//				let table = "<table class='table table-stripped mt-3'>";
//			    table += "<tr> <th>Id</th> <th >Name</th> <th>Nama Singkat</th> <th>Action</th>"
//				if (result.length > 0)
//				{
//					for (let i = 0; i < result.length; i++) {
//						table += "<tr>";
//						table += "<td>" + result[i].id + "</td>";
//                        table += "<td>" + result[i].name + "</td>";
//                        table += "<td>" + result[i].abbreviation + "</td>";
//                        table += "<td><button class='btn btn-warning' value='" + result[i].id + "' onclick=editLocationLevel(this.value)><i class='bi-pencil-square'></i></button> <button class='btn btn-danger' value='" + result[i].id + "' onclick=deleteLocationLevel(this.value)><i class='bi-trash'></i></button></td>";
//						table += "</tr>";
//					}
//				} else {
//					table += "<tr>";
//					table += "<td colspan='2' class='text-center'>No data</td>";
//					table += "</tr>";
//				}
//				table += "</table>";
//				$('#locationLevelTable').html(table);
//			}
//		});
//	} else {
//		GetAllLocationLevel();
//	}
//}

function SearchLocation(request) {
//console.log(request)

    if (request.length > 0)
	{
	    $.ajax({
			url: '/api/searchlocation/' + request,
			type: 'GET',
			contentType: 'application/json',
			success: function (result) {
			    //console.log(result)
			    let table = "<table class='table table-bordered mt-3'>";
				table += "<tr> <th width='10%' class='text-center'>ID </th> <th>Nama</th> <th>Level Lokasi</th> <th>Wilayah</th> <th>Action</th></tr>"
			    if (result.length > 0)
			    {
				    for (let i = 0; i < result.length; i++) {
					    table += "<tr>";
					    table += "<td class='text-center'>" + (i+1) + "</td>";
                        table += "<td>" + result[i].name + "</td>";
                        table += "<td>" + (result[i].locationLevel).name  + "</td>";
                        table += "<td>" + result[i].name + "</td>";
                        table += "<td><button class='btn btn-primary btn-sm' value='" + result[i].id + "' onclick=editLocation(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" +  result[i].id + "' onclick=deleteLocation(this.value)>Delete</button></td>";

						table += "</tr>";
					}
				} else {
				    table += "<tr>";
				    table += "<td colspan='5' class='text-center'>No data</td>";
				    table += "</tr>";
		        }
				table += "</table>";
				$('#locationList').html(table);
			}
		});
	} else {
        LocationList(0,5);
	}
}
function sortbylevel(){
        $.ajax({
            url: "/api/getalllocationlevel",
            type: "GET",
            contentType: "application/json",
            success: function(data) {
                for (let i = 0; i < data.length; i++) {
                    $("#sortByLevel").append(
                        `<option value="${data[i].id}">${data[i].name}</option>`
                    );
                    console.log("ID dari location level:", data[i].id);
                }
            }
        });
}
function sortByLocationLevel(levelId) {
    if (levelId.toLowerCase() === "all") {
        LocationList(0, 5);
        $('#pagination').show(); // Display pagination
        enablePagination();
        return;
    } else {
        $.ajax({
            url: "/api/location/byLevel/" + levelId,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                if (data && Array.isArray(data)) {
                    let table = '<table class="table table-bordered mt-3">';
                    table += '<tr> <th>Id</th> <th>Name</th> <th>Level Lokasi</th> <th>Wilayah</th> <th>Action</th> </tr>';
                    for (let i = 0; i < data.length; i++) {
                        table += "<tr>";
                        table += "<td class='text-center'>" + (i + 1) + "</td>";
                        table += "<td>" + data[i].name + "</td>";
                        table += "<td>" + data[i].locationLevel.name + "</td>";
                        table += '<td>';
                        table += data[i].parentLocation ? `${data[i].parentLocation.locationLevel.abbreviation} ${data[i].parentLocation.name}` : 'N/A';
                        table += '</td>';
                        table += "<td><button class='btn btn-primary btn-sm' value='" + data[i].id + "' onclick=editlocation(this.value)>Edit</button> <button class='btn btn-danger btn-sm' value='" + data[i].id + "' onclick=deletelocation(this.value)>Delete</button></td>";
                        table += "</tr>";
                    }
                    table += "</table>";
                    $('#locationList').html(table);
                    disablePagination();  // assuming you want to disable pagination for sorted results
                } else {
                    $('#locationList').html(
                        "<table class='table table-bordered mt-3'>" +
                        "<tr><td colspan='5' class='text-center'>No Data</td></tr>" +
                        "</table>"
                    );
                }
                $('#pagination').hide(); // Hide pagination
            },
            error: function (error) {
                console.error('Error occurred:', error);
            }
        });
    }
}


function disablePagination() {
    $('.page-link:contains("Next")').addClass('disabled');
    $('.page-link:contains("Previous")').addClass('disabled');
    $('.page-link').addClass('disabled');
}

function enablePagination() {
    $('.page-link:contains("Next")').removeClass('disabled');
    $('.page-link:contains("Previous")').removeClass('disabled');
    $('.page-link').removeClass('disabled');
}


$(document).ready(function(){
LocationList(0,5);
sortbylevel();
});