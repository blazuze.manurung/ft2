function setCountDown(){
    setCountDown = new Date().getTime() + 1800000;
    timeDown = setInterval(cointDownHtml(), 1000);
    Loading(false);
}

var countDownDate = new Date().getTime() + 1800000;
var email = $("#saveEmail").val();
var timeDown = setInterval(cointDownHtml, 1000);

function cointDownHtml(){
    document.getElementById("textUlang").innerHTML = "OTP akan dikirim dalam";
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var menit = String (Math.floor((distance %(1000 * 60 * 60)) / (1000 * 60)));
    var detik = String (Math.floor((distance %(1000 * 60)) / 1000));

    if (menit.length != 2){
        menit = "0" + menit;
    }
    if (detik.length != 2){
        detik = "0" + detik;
    }

    document.getElementById("timeDelay").innerHTML = menit +" : " + detik;

    if (distance < 0){
        clearInterval(timeDown);
        document.getElementById("textUlang").innerHTML = "Kirim ulang kode OTP"
        document.getElementById("timeDelay").innerHTML"<a href='#' onclick='newOTP(email)'>Here</a>";
    }
}

function getOTP(emailInput){
    Loading(true);
    var token = "";
    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for ( let i = 0; i < 8; i++ ) {
            token += characters.charAt(Math.floor(Math.random() * characters.length));
        }
        var otpMessage = "Kode OTP kamu: "+ token +". OTP ini bersifat RAHASIA harap tidak dibagikan ke siapapun. Silakan masukan kode ini untuk pendaftaran kamu.\n Jika merasa tidak melakukan pendaftaran abaikan mail ini Terima Kasih";
        var templateSent = {};
        templateSent.subject = "OTP Register PIN";
        templateSent.message = otpMessage;
        var myJson = JSON.stringify(templateSent);
        $.ajax({
            url: "api/mail/send/?mailTo=" + emailInput,
            type: "POST",
            contentType: "application/json",
            data: myJson,
            success: function(data){
               if(data === "Berhasil"){
                   postToken(emailInput, token);
               }else{
                   alert("Server gagal mengirimkan OTP");
               }
            }
        });
    }
function postToken(email, token){
    var data = {};
    data.email = email;
    data.token = token;
    data.is_expired = false;
    data.used_for = "Pendaftaran";
    var myJson = JSON.stringify(data);
    $.ajax({
        url : "/api/token/save",
        type : "POST",
        contentType : "application/json",
        data : myJson,
        success : function(data){
            setCountDown();
        },
        error: function(){
            setCountDown();
            alert("Terjadi kesalahan")
            return;
        }
    });
}

function Loading(keterangan){
    if(keterangan === true){
        document.getElementById("divLoading").hidden = false;
        document.getElementById("textUlang").hidden = true;
        document.getElementById("timeDelay").hidden = true;
   }else{
        document.getElementById("divLoading").hidden = true;
        document.getElementById("textUlang").hidden = false;
        document.getElementById("timeDelay").hidden = false;
    }
}

$("#btnKirimOtp").click(function(){
    var email = $("#saveEmail").val();
    var tokenInput = $("#verifOtp").val();
    if(tokenInput === ""){
        $("#verifOtpErr").text("Masukkan Kode OTP");
        return
    }else{
        $("#verifOtpErr").text("");
    }

    $.ajax({
        url: "api/token/lastToken/?email=" + email + "&used_for=Pendaftaran&input=" + tokenInput,
        type: "GET",
        contentType: "application/json",
        success: function(data){
           if(data === "aman"){
               clearInterval(timeDown);
               toExpiredOTP(email);
           }else if(data === "kadaluarsa"){
               $("#verifOtpErr").text("*Kode OTP kadaluarsa, silakan kirim ulang OTP");
               return;
           }else if(data === "salah"){
               $("#verifOtpErr").text("*Kode OTP salah");
               return;
           }
        }
    });
})

function toExpiredOTP(email){
    $.ajax({
        url: "api/token/tokenToExpired/?email="+email+"&used_for=Pendaftaran",
        type: "PUT",
        contentType: "application/json",
        success: function(data){
            moveToPassword(email);
        }
    });
}

function moveToPassword(email){
    $.ajax({
        url: "/register/setPassword/"+email,
        type: "GET",
        contentType: "html",
        success: function(data){
            $(".modal").modal("hide");
            $(".modal-title").text("Set Password");
            $(".modal-body").html(data);
            $(".modal").modal("show");
        }
    });
}

$(".modal").on("hidden.bs.modal", function(){
    clearInterval(timeDown);
    $(".modal-body1").html("");
})