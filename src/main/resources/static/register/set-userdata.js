setRole();
function setRole(){
    var roleSelect = document.getElementById("roleSelect");
    $.ajax({
        url : "/api/getallrole",
        type : "GET",
        contentType : "application/json",
        success: function(data){
            for(i = 0; i<data.length; i++){
                roleSelect.options[roleSelect.options.length] = new Option(data[i].name, data[i].id);
            }
        }
    });
}

$("#btnDaftarFinal").click(function(){
    buttonLoading(true);
    var fullname = $("#fullnameInput").val();
    var phoneNumber = $("#phoneNumber").val();
    var role = $("#roleSelect").val();
    var booleanFullname = false;
    var booleanRole = false;

    if(fullname === ""){
        $("#fullnameInputErr").text("*Nama lengkap tidak boleh kosong");
    }else{
        $("#fullnameInputErr").text("");
        booleanFullname = true;
    }
    if(role === ""){
        $("#roleSelectErr").text("*Pilih role yang tersedia");
    }else{
        $("#roleSelectErr").text("");
        booleanRole = true;
    }

    if(booleanFullname == false || booleanRole == false){
        buttonLoading(false);
        return;
    }
    else if(booleanFullname == true && booleanRole == true){
        var objBiodata = {};
        objBiodata.fullname = fullname;
        objBiodata.mobile_phone = phoneNumber;
        var myJson = JSON.stringify(objBiodata);
        $.ajax({
            url : "/api/biodata/callId",
            type : "POST",
            data : myJson,
            contentType : "application/json",
            success: function(data){
                setUser(data);
            },
            error: function(){
                buttonLoading(false);
                alert("Terjadi kesalahan");
            }
        });
    }
})

function setUser(data){
    var email = $("#saveEmail").val();
    var password = $("#savePassword").val();
    var role = $("#roleSelect").val();
    var objUser = {};
    objUser.biodata_id = data;
    objUser.role_id = role;
    objUser.email = email;
    objUser.password = password;
    objUser.is_locked = false;
    var myJson = JSON.stringify(objUser);
    $.ajax({
        url : "/api/user/callResponse",
        type : "POST",
        data : myJson,
        contentType : "application/json",
        success: function(data){
            moveInfo();
        },
        error: function(){
            buttonLoading(false);
            alert("Terjadi kesalahan");
        }
    });
}

function moveInfo(){
    $.ajax({
        url : "/register/info-register",
        type : "GET",
        contentType : "html",
        success: function(data){
             $(".modal").modal("hide");
             $(".modal-title").text("Informasi");
             $(".modal-body").html(data);
             $(".modal").modal("show");
        }
    });
}

function buttonLoading(keterangan){
    if(keterangan === true){
        document.getElementById("btnDaftarFinal").disabled = true;
        document.getElementById("btnDaftarFinal").innerHTML = "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> Loading...";
    }else{
        document.getElementById("btnDaftarFinal").disabled = false;
        document.getElementById("btnDaftarFinal").innerHTML = "Daftar";
    }
}

